package cn.desert.newpos.payui.master;

import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.newpos.pay.R;
import com.android.newpos.pay.StartAppDATAFAST;
import com.datafast.menus.menus;
import com.newpos.libpay.Logger;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.pos.device.icc.IccReader;
import com.pos.device.icc.SlotType;

import java.util.Timer;
import java.util.TimerTask;

import cn.desert.newpos.payui.base.BaseActivity;

import static com.android.newpos.pay.StartAppDATAFAST.makeinitcallback;
import static com.datafast.menus.MenuAction.callBackSeatle;
import static java.lang.Thread.sleep;

/**
 * Created by zhouqiang on 2016/11/12.
 */
public class ResultControl extends BaseActivity {
    Button confirm ;
    TextView details ;
    ImageView face ;
    ImageView removeCard ;
    IccReader iccReader0;
    Thread proceso = null;
    private static long second = 1000;
    private final int TIMEOUT_REMOVE_CARD = 60 * 2000;//2 min
    private Timer timer = null;
    private String info = null ;
    private boolean buttonActive = false;
    private boolean flag;
    private static long time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trans_result);
        setReturnVisible(View.INVISIBLE);
        setNaviTitle(R.string.trans_result);
        setRightVisiblity(View.GONE);
        confirm = (Button) findViewById(R.id.result_confirm);
        details = (TextView) findViewById(R.id.result_details);
        face = (ImageView) findViewById(R.id.result_img);
        iccReader0 = IccReader.getInstance(SlotType.USER_CARD);

        timer = new Timer() ;

        if (FinanceTrans.isMsgVip){
            FinanceTrans.isMsgVip = false;
            time = 7 * second;
        }else {
            time = 3 * second;
        }

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            displayDetails(bundle.getBoolean("flag"), bundle.getString("info"));
            if (bundle.getBoolean("boton") ){
                confirm.setVisibility(View.VISIBLE);
                buttonActive = true;
            } else {
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        //over();
                        removeCard();
                    }
                } , time);
            }
            over();
        } else {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    //over();
                    removeCard();
                }
            } , time);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
        if(timer!=null){
            timer.cancel();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            over();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(KeyEvent.changeAction(event, KeyEvent.KEYCODE_ENTER));
    }

    private void over(){
        //finish();
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //over();
                goMenus();
                removeCard();
            }
        });
    }

    private void displayDetails(boolean flag , String info){
        this.info = info;
        this.flag = flag;
        details.setText(info);
        if(flag){
            face.setImageResource(R.drawable.result_success);
            details.setTextColor(Color.parseColor("#0097AC"));
        }else {
            face.setImageResource(R.drawable.result_fail);
            details.setTextColor(Color.parseColor("#0097AC"));
        }
    }

    @Nullable
    @Override
    public CharSequence onCreateDescription() {
        return super.onCreateDescription();
    }

    private void removeCard(){
        if (buttonActive && flag) {
        menus.pause = false;
        startActivity(new Intent(ResultControl.this, StartAppDATAFAST.class));
    }
        if (callBackSeatle != null)
            callBackSeatle.getRspSeatleReport(0);

        finish();
//        if (proceso == null) {
//            proceso = new Thread(new Runnable() {
//                public void run() {
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (iccReader0.isCardPresent()) {
//                                setContentView(R.layout.activity_remove_card);
//                                removeCard = (ImageView) findViewById(R.id.iv_remove__card);
//                                removeCard.setImageResource(R.drawable.remove_card);
//                            }
//                        }
//                    });
//
//                    if (checkCard()){
//                        if (buttonActive && flag) {
//                            menus.pause = false;
//                            startActivity(new Intent(ResultControl.this, StartAppDATAFAST.class));
//                        }
//                        if (callBackSeatle != null)
//                            callBackSeatle.getRspSeatleReport(0);
//
//                        finish();
//                    }
//                }
//            });
//            proceso.start();
//        }
    }

    private boolean checkCard(){
        boolean ret = false;
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        //iccReader0 = IccReader.getInstance(SlotType.USER_CARD);

        long start = SystemClock.uptimeMillis() ;

        while (true) {

            try {
                if (iccReader0.isCardPresent()) {
                    toneG.startTone(ToneGenerator.TONE_PROP_BEEP2, 2000);
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        Logger.error("Exception" + e.toString());
                        Thread.currentThread().interrupt();
                    }

                    if (SystemClock.uptimeMillis() - start > TIMEOUT_REMOVE_CARD) {
                        toneG.stopTone();
                        ret = true;
                        break;
                    }
                } else {
                    ret = true;
                    break;
                }
            }catch (Exception e){
                ret = true;
                break;
            }
        }

        proceso = null;
        return ret;
    }

    private void goMenus() {
        if (makeinitcallback != null) {
            makeinitcallback.getMakeInitCallback(true);
        }
    }
}
