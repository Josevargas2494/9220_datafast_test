package com.datafast.definesDATAFAST;

public final class DefinesDATAFAST {

    //=======================Datafast=======================//

    public static final String DATO_MENU = "MENU";
    public static final String ITEM_TRANSACCIONES = "TRANSACCIONES";
    public static final String ITEM_CONFIGURACION = "CONFIGURACION";
    public static final String ITEM_COMERCIO = "COMERCIO";
    public static final String ITEM_PRINCIPAL = "PRINCIPAL";
    public static final String ITEM_RE_IMPRESION = "RE IMPRESION";
    public static final String ITEM_ANULACION = "ANULACION";
    public static final String ITEM_DEPOSITO = "CIERRE";
    public static final String ITEM_ECHO_TEST = "ECHO TEST";
    public static final String ITEM_BORRAR_LOTE = "BORRAR LOTE";
    public static final String ITEM_BORRAR_REVERSO = "BORRAR REVERSO";
    public static final String ITEM_REPORTE_DETALLADO = "REPORTE DETALLADO";
    public static final String ITEM_PAGOS_ELECTRONICOS = "PAGOS CON CODIGO";
    public static final String ITEM_TIPO_COMUNICACION = "TIPO DE COMUNICACION";
    public static final String ITEM_INICIALIZACION = "INICIALIZACION \n POLARIS";
    public static final String ITEM_CONFIG_INICIAL = "CONFIGURACION \n POLARIS";
    public static final String ITEM_CONFIG_WIFI = "CONFIGURACION \n WIFI";
    public static final String ITEM_APPMANAGER = "APPMANAGER";
    public static final String GERCARD_MSG_SWIPE_ICC_CTL = "DESLICE/INSERTE O \n ACERQUE  LA TARJETA";
    public static final String GERCARD_MSG_FALLBACK = "PASE LA TARJETA";
    public static final String GERCARD_MSG_ICC_CTL = "INSERTE O ACERQUE  \n TARJETA";
    public static final String GERCARD_MSG_SWIPE = "DESLICE LA TARJETA";
    public static final String GERCARD_MSG_ICC = "INSERTE LA TARJETA";
    public static final String GERCARD_MSG_CTL = "ACERQUE LA TARJETA";
    public static final String GERCARD_MSG_TOKEN_PE = "PASE O INGRESE \n TOKEN";
    public static final String TITULO_ANULACION = "INGRESE NUMERO DE REFERENCIA";
    public static final String TIPO_LAYOUT_GRID = "GRID";
    public static final String TIPO_LAYOUT_LINEAR = "LINEAR";
    public static final String ITEM_IMPRESION = "IMPRESION";
    public static final String ITEM_DIFERIDO = "DIFERIDO";
    public static final String ITEM_TEST = "TEST";
    public static final String ITEM_VENTA = "VENTA";
    public static final String ITEM_PRE_AUTORIZACION = "PRE AUTORIZACION";
    public static final String ITEM_PAGOS_VARIOS = "PAGOS VARIOS";
    public static final String ITEM_PRE_VOUCHER = "PREVOUCHER";
    public static final String ITEM_PAGO_PREVOUCHER = "PAGO \n PREVOUCHER";
    public static final String ITEM_CASH_OVER = "CASH OVER";
    public static final String ITEM_TRANS_PRE_AUT = "PRE AUTORIZACIÓN";
    public static final String ITEM_AMPLIACION = "AMPLIACION";
    public static final String ITEM_CONFIRMACION = "CONFIRMACION";
    public static final String ITEM_ANULACION_PRE_AUT = "ANULACION PRE AUT";
    public static final String ITEM_REIMPRESION_PRE_AUT = "REIMPRESION ANUL PRE AUT";
    public static final String MSG_BATTERY = "BATERIA BAJA \n CONECTE CARGADOR";
    public static final String MSG_PAPER = "INSERTE PAPEL \n PARA CONTINUAR";
    public static final String LOTE_VACIO = "LOTE VACIO";
    public static final String LOTE_VACIO_PREAUTO = "SIN PREAUTORIZACIONES";
    public static final String MSG_SETTLE = "CIERRE LOTE PARA CONTINUAR";
    public static final String FILE_NAME_PREAUTO = "_preauto_";
    public static final String FILE_NAME_PREVOUCHER = "_prevoucher_";
    public static final String ITEM_TRANS_EN_PANTALLA = "TRANSACCIONES EN LOTE";
    public static final String ITEM_COMUNICACION = "COMUNICACION";
    public static final String ITEM_PREAUTO_PANTALLA = "PREAUTORIZACION EN LOTE";
    public static final String ITEM_ULTIMO_CIERRE = "ULTIMO CIERRE";
    public static final String ITEM_MENU_OPERARIO = "MENU OPERARIO";
    public static final String ITEM_MODE_KIOSKO = "MODO KIOSKO";
    public static final String MSG_REV_PEN = "REVERSO PENDIENTE";
    public static final String NAME_TRANS_PE = "PAGO CON CODIGO DIFERIDO";


    public static final String NAME_FOLDER_CTL_FILES = "CTL_Files";
    public static final String NAME_FOLDER_CTL_CAPKS = "CTL_Cakps";
    public static final String ENTRY_POINT = "ENTRY_POINT";
    public static final String PROCESSING = "PROCESSING";
    public static final String TERMINAL = "TERMINAL";
    public static final String CAKEY = "CAKEY";
    public static final String REVOK = "REVOK";




}
