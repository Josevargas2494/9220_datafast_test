package com.datafast.mdm_validation;

import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static com.android.newpos.pay.StartAppDATAFAST.readWriteFileMDM;

public class ReadWriteFileMDM {

    private XmlSerializer serializer;
    private FileOutputStream fileos = null;
    private String Reverse;
    private String Settle;
    private String initAuto;

    public final String REVERSE_ACTIVE = "1";
    public final String TRANS_ACTIVE = "1";
    public final String REVERSE_DEACTIVE = "0";
    public final String TRANS_DEACTIVE = "0";
    public final String INITAUTOACTIVE = "1";
    public final String INITAUTODEACTIVE = "0";


    public static ReadWriteFileMDM getInstance(){
        if (readWriteFileMDM == null){
            readWriteFileMDM = new ReadWriteFileMDM();
            readWriteFileMDM.readFileMDM();
        }
        return readWriteFileMDM;
    }

    public String getReverse() {
        return Reverse;
    }

    public void setReverse(String reverse) {
        Reverse = reverse;
    }

    public String getSettle() {
        return Settle;
    }

    public void setSettle(String settle) {
        Settle = settle;
    }

    public String getInitAuto() {
        return initAuto;
    }

    public void setInitAuto(String initAuto) {
        this.initAuto = initAuto;
    }

    public void createXML(String name) {

        serializer = Xml.newSerializer();
        File newxmlfile;
        if(Build.MODEL.equals("NEW9220")) {
            newxmlfile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + name + ".xml");
        }
        else {
            newxmlfile = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()  , name + ".xml");
        }

        try {
            newxmlfile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            fileos = new FileOutputStream(newxmlfile);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {

            serializer.setOutput(fileos, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTag(String tag, String value) {
        if (value != null) {
            try {
                serializer.startTag(null, tag);
                serializer.text(value);
                serializer.endTag(null, tag);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeFileMDM(String reversal, String settle, String init) {

        createXML("ConfigDF");

        try {
            serializer.startTag(null, "Configuration");
            serializer.startTag(null, "MdmInstall");

            setTag("isReversal", reversal);
            setTag("isSettle", settle);
            setTag("initAuto", init);

            serializer.endTag(null, "MdmInstall");
            serializer.endTag(null, "Configuration");

            serializer.endDocument();
            serializer.flush();
            fileos.close();

            readFileMDM();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void readFileMDM() {

        String  reverse = null,
                init = null,
                settle=null;

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/ConfigDF.xml");


        if (file.exists()) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            try {

                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(file);
                Element documentElement = document.getDocumentElement();
                NodeList sList = documentElement.getElementsByTagName("MdmInstall");

                if (sList != null && sList.getLength() > 0) {
                    for (int i = 0; i < sList.getLength(); i++) {
                        Node node = sList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {

                            node.getAttributes();
                            Element e = (Element) node;
                            NodeList listElements = e.getElementsByTagNameNS(node.getNamespaceURI(), node.getLocalName());

                            //lee cada elemento del state
                            for (int b = 0; b < listElements.getLength(); b++) {

                                String nameEl = listElements.item(b).getNodeName();
                                try {
                                    if (nameEl.equals("isReversal")) {
                                        reverse = listElements.item(b).getTextContent();
                                    }
                                    if (nameEl.equals("isSettle")) {
                                        settle = listElements.item(b).getTextContent();
                                    }
                                    if (nameEl.equals("initAuto")) {
                                        init = listElements.item(b).getTextContent();
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();

                                }

                            }
                            if (reverse != null && settle != null && init != null) {
                                Reverse=reverse;
                                Settle=settle;
                                initAuto=init;
                            } else {
                                writeFileMDM(REVERSE_DEACTIVE, TRANS_DEACTIVE,INITAUTODEACTIVE);
                            }

                        }
                    }
                }
            } catch (Exception e) {
                createXML("ConfigDF");
            }
        } else {
            writeFileMDM(REVERSE_DEACTIVE, TRANS_DEACTIVE, INITAUTODEACTIVE);
        }

    }
}
