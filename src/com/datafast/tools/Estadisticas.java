package com.datafast.tools;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.telephony.CellInfo;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import com.newpos.libpay.utils.PAYUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.List;

public class Estadisticas {
    private static  boolean sdDisponible = false;
    private static boolean sdAccesoEscritura = false;
    public  static String Date = null;
    private  static int countransex = 0;
    private static  int countranserro = 0;
    private  static int counterroimpre = 0;
    private static  int countreverso = 0;
    private static  int counterroreverso = 0;
    private static int countsinResp = 0;
    private  static int countLectBanda = 0;
    private static int countLectChip = 0;
    private static int countLectNfc = 0;
    private static int countLectHand = 0;
    private static  long totalbateria = 0;
    private static long nivelSenal = 0;
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    public static String TRANS_SUCCESS = "countTrans";
    public static String TRANS_FAIL = "countTransError";
    public static String TRANS_REVERSE_FAIL = "countReversoError";
    public static String TRANS_SIN_RESP = "coutTransinResp";
    public static String TRANS_IMP_ERR = "countImpreError";
    public static String TRANS_LECT_BANDA = "countlectBanda";
    public static String TRANS_LECT_CHIP = "countlectchip";
    public static String TRANS_LECT_NFC = "countlectnfc";
    public static String TRANS_LECT_HAND = "countlecthand";


    public static void verificacionArchivo(String estado) {
        if (estado.equals(Environment.MEDIA_MOUNTED))
        {
            sdDisponible = true;
            sdAccesoEscritura = true;
        }
        else if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
        {
            sdDisponible = true;
            sdAccesoEscritura = false;
        }
        else
        {
            sdDisponible = false;
            sdAccesoEscritura = false;
        }
    }


    public static String readArchive() {
        String Informacion = null;

        if(sdDisponible){

            try
            {
                File ruta_sd = Environment.getExternalStorageDirectory();
                File f = new File(ruta_sd.getAbsolutePath(), "fichero" + PAYUtils.getLocalDate() + ".txt");
                BufferedReader fin =
                        new BufferedReader(
                                new InputStreamReader(
                                        new FileInputStream(f)));
                String texto = fin.readLine();
                Informacion = texto.replace("/","\n");
                fin.close();
            }
            catch (Exception ex)
            {
                Log.e("Ficheros", "Error al leer fichero");
            }

        }
        return Informacion;
    }

    public static void writearchive(String Fecha, long nivelbateria, String nivelseñal, long numTransAprobada, long totalTransRechazada,
                                    long totalReverso, long totaltransinResp, long totalimprErro, long totalbanda, long totallectchip, Context context ) {

        String informacion = null, informacionAnterior;

        if(sdAccesoEscritura && sdDisponible){
            File ruta_sd = Environment.getExternalStorageDirectory();
            File f = new File(ruta_sd.getAbsolutePath(), "fichero" + PAYUtils.getLocalDateAAAAMMDD() + ".txt");
            if(sdAccesoEscritura && sdDisponible){
                try
                {
                    OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(f));

                    /*System.out.println("Nivel de Bateria "+nivelbateria);
                    System.out.println("Numero de Transacciones "+numTransAprobada);
                    long valor = Long.parseLong(nivelbateria) * (numTransAprobada - 1)+Long.parseLong(nivelbateria);*/

                    System.out.println("Total de Bateria "+totalbateria);

                    System.out.println("Num transacciones " + numTransAprobada);

                    int Bateria = (int) (totalbateria/numTransAprobada);

                    System.out.println("Valor promedio de Bateria "+Bateria);

                    almacenarDatos("bateria",totalbateria,context);

                    nivelSenal += Integer.parseInt(nivelseñal);

                    almacenarDatos("señal",nivelSenal,context);

                    long senalTotal = nivelSenal/(numTransAprobada+totalTransRechazada+totalReverso+totaltransinResp+totalimprErro+totalbanda+totallectchip);

                    informacion = Fecha+",1,U,"+Bateria+";"+Fecha+",2,U,"+senalTotal+";"+Fecha+",3,U,"+numTransAprobada+";"+Fecha+",4,U,"+totalTransRechazada+";"+Fecha+",5,U,"+totalReverso+";"+Fecha+",6,U,"+totaltransinResp+";"+Fecha+",7,U,"+totalimprErro+";"+Fecha+",8,U,"+totalbanda+";"+Fecha+",9,U,"+totallectchip+";";

                    fout.write(informacion);
                    fout.close();
                }
                catch (Exception ex)
                { Log.e("Ficheros", "Error al escribir fichero "); }
            }

        }

    }

    public static void countsTransaction(String contador, Context context ){

        /*String estado = Environment.getExternalStorageState();
        Estadisticas.verificacionArchivo(estado);
        String fecha = PAYUtils.getLocalDateAAAAMMDD();
        int bateria = read_battery(context);
        int señal = readSignal(context);

        try{
            llenarVariables(context);
        }catch (Exception e){
            e.printStackTrace();
        }

        switch (contador){
            case "countTrans":
                totalbateria += read_battery(context);
                countransex++;
                break;

            case "countTransError":
                countranserro++;
                break;

            case "countReverso":
                countreverso++;

            case "countReversoError":
                counterroreverso++;
                break;

            case "coutTransinResp":
                countsinResp++;
                break;

            case "countImpreError":
                counterroimpre++;
                break;

            case "countlectBanda":
                countLectBanda++;
                break;

            case "countlectchip":
                countLectChip++;
                break;

            case "countlectnfc":
                countLectNfc++;
                break;

            case "countlecthand":
                countLectHand++;
                break;
        }

        writearchive(fecha,totalbateria, String.valueOf(señal),countransex,countranserro,counterroreverso,countsinResp,counterroimpre,countLectBanda,countLectChip, context);*/

    }

    private static void llenarVariables(Context context) {
        String file;

        if (totalbateria <= 0) {
            totalbateria = obtenerDatos("bateria", context);
            System.out.println("TOTAL BATERIA dentro ------------" +totalbateria);
        }
        System.out.println("TOTAL BATERIA ------------" +totalbateria);

        if (nivelSenal <= 0) {
            nivelSenal = obtenerDatos("señal", context);
            System.out.println("SEÑAL dentro------------" +nivelSenal);
        }
        System.out.println("SEÑAL------------" +nivelSenal);

        file = readArchive();
        if (file != null){
            System.out.println("FILE ****************" + file);
            String[] datos = file.split(";");

            for (int i = 0; i < datos.length; i ++){
                String[] valor = datos[i].split(",");
                int valorLeido = Integer.parseInt( valor[3] );

                switch (valor[1]){
                    case "3":
                        countransex = valorLeido;
                        System.out.println("VALOR 3 ***" + countransex);
                        break;

                    case "4":
                        countranserro = valorLeido;
                        System.out.println("VALOR 4 ***" + countranserro);
                        break;

                    case "5":
                        counterroreverso = valorLeido;
                        System.out.println("VALOR 6 ***" + counterroreverso);
                        break;

                    case "6":
                        countsinResp = valorLeido;
                        System.out.println("VALOR 7 ***" + countsinResp);
                        break;

                    case "7":
                        counterroimpre = valorLeido;
                        System.out.println("VALOR 8 ***" + counterroimpre);
                        break;

                    case "8":
                        countLectBanda = valorLeido;
                        System.out.println("VALOR 9 ***" + countLectBanda);
                        break;

                    case "9":
                        countLectChip = valorLeido;
                        System.out.println("VALOR 10 ***" + countLectChip);
                        break;

                    /*case "10":
                    countreverso = valorLeido;
                    System.out.println("VALOR 5 ***" + countreverso);
                    break;

                    case "11":
                        countLectNfc = valorLeido;
                        System.out.println("VALOR 11 *** " + countLectNfc);
                        break;

                    case "12":
                        countLectHand = valorLeido;
                        System.out.println("VALOR 12 *** " + countLectHand);
                        break;*/

                }
            }
        }
    }

    public static int read_battery(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

        return level;
    }

    private static int readSignal(Context context){
        String networkOperator = "";
        GsmCellLocation location;
        int signal = 0;

        TelephonyManager tel = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return 0;
        }

        try {
            List<CellInfo> cellList = tel.getAllCellInfo();
            CellInfoWcdma cellinfoWcdma = null;
            CellInfoLte cellInfoLte = null;

            if (cellList != null && !cellList.isEmpty()) {

                String cellInfo = String.valueOf(cellList);
                System.out.println("celllist 1********************" + cellList);
                System.out.println("cellInfo********************" + cellInfo);

                if (cellInfo.indexOf("CellSignalStrengthWcdma") != -1) {
                    try {
                        cellinfoWcdma = (CellInfoWcdma) tel.getAllCellInfo().get(0);
                        CellSignalStrengthWcdma cellSignalStrengthWcdma = cellinfoWcdma.getCellSignalStrength();
                        signal = cellSignalStrengthWcdma.getDbm();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (cellInfo.indexOf("CellSignalStrengthLte") != -1) {
                    try {
                        cellInfoLte = (CellInfoLte) tel.getAllCellInfo().get(0);
                        CellSignalStrengthLte cellSignalStrengthLte = cellInfoLte.getCellSignalStrength();
                        signal = cellSignalStrengthLte.getDbm();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            return Math.abs(signal);
        }catch (Exception e){
            return 0;
        }

    }
    public static void createFile(Context context) {
        String estado = Environment.getExternalStorageState();
        Estadisticas.verificacionArchivo(estado);
        File ruta_sd = Environment.getExternalStorageDirectory();
        File f = new File(ruta_sd.getAbsolutePath(), "fichero" + PAYUtils.getLocalDateAAAAMMDD() + ".txt");
        if (!f.exists()) {
            almacenarDatos("bateria",0,context);
            almacenarDatos("señal",0,context);
            OutputStreamWriter fout;
            try {
                fout = new OutputStreamWriter(new FileOutputStream(f));
                fout.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void almacenarDatos(String key, long valor , Context context){
        SharedPreferences preferencias=context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putLong(key,valor);
        editor.apply();
    }

    public static void almacenarFecha(String key, String fecha , Context context){
        SharedPreferences preferencias=context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString(key,fecha);
        editor.apply();
    }

    public static String obtenerFecha(String key, Context context){
        SharedPreferences prefe=context.getSharedPreferences(key, Context.MODE_PRIVATE);
        String date = prefe.getString(key,"");
        if (date.equals("")) {
            date = PAYUtils.getLocalDateAAAAMMDD();
            almacenarFecha("fecha", date, context);
        }
        return date;
    }

    public static long obtenerDatos(String key, Context context){
        SharedPreferences prefe=context.getSharedPreferences(key, Context.MODE_PRIVATE);
        long valor = prefe.getLong(key,0);
        return valor;
    }

    public static boolean deleteFile() {
        String estado = Environment.getExternalStorageState();
        Estadisticas.verificacionArchivo(estado);
        File ruta_sd = Environment.getExternalStorageDirectory();
        File f = new File(ruta_sd.getAbsolutePath(), "ficherosd.txt");
        boolean fDelete = false;
        if (f.exists()) {
            fDelete = f.delete();
        }
        return fDelete;
    }

    public static boolean compareDate(Context context) {
        boolean datePass = false;

        java.util.Date fechaInicial= null, fechaFinal = null;

        try {
            fechaInicial = dateFormat.parse(obtenerFecha("fecha", context));
            fechaFinal = dateFormat.parse(PAYUtils.getLocalDateAAAAMMDD());
        } catch (Exception e) {
            e.printStackTrace();
        }

        int dias=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);

        System.out.println("Hay " + dias + " dias de diferencia");
        if (dias > 0) {
            almacenarFecha("fecha", PAYUtils.getLocalDateAAAAMMDD(), context);
            datePass = true;
        }

        return datePass;
    }
}
