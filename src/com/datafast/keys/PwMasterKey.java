package com.datafast.keys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.newpos.pay.R;

import cn.desert.newpos.payui.UIUtils;

public class PwMasterKey extends AppCompatActivity {

    private Button btn_ok, btn_cnl;
    private EditText et_pw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pw_master_key);

        et_pw = (EditText)findViewById(R.id.et_pw_mk);
        btn_ok = (Button) findViewById(R.id.btn_conf_mon);
        btn_cnl = (Button) findViewById(R.id.btn_cancel_mon);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_pw.getText().length() != 0) {
                    Intent intent = new Intent();
                    intent.setClass(PwMasterKey.this, InjectMasterKey.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putString("pw", et_pw.getText().toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
                else{
                    UIUtils.toast(PwMasterKey.this, R.drawable.ic_launcher, getString(R.string.err_msg_pwmk), Toast.LENGTH_SHORT);
                }
            }
        });

        btn_cnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish();
            }
        });
    }

    public void onResume() {
        super.onResume();
        et_pw.setText("");
    }
}
