package com.datafast.REST.response;

public class RspVIP {

    public final String T_FNC = "FNC";
    public final String T_TID = "TID";
    public final String T_REF = "REF";
    public final String T_VAL = "VAL";
    public final String T_MSJ = "MSJ";
    public final String T_F73 = "F73";

    private String FNC;
    private String TID;
    private String REF;
    private String VAL;
    private String MSJ;
    private String F73;

    public String getFNC() {
        return FNC;
    }

    public void setFNC(String FNC) {
        this.FNC = FNC;
    }

    public String getTID() {
        return TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public String getREF() {
        return REF;
    }

    public void setREF(String REF) {
        this.REF = REF;
    }

    public String getVAL() {
        return VAL;
    }

    public void setVAL(String VAL) {
        this.VAL = VAL;
    }

    public String getMSJ() {
        return MSJ;
    }

    public void setMSJ(String MSJ) {
        this.MSJ = MSJ;
    }

    public String getF73() {
        return F73;
    }

    public void setF73(String f73) {
        F73 = f73;
    }
}
