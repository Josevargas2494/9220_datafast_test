package com.datafast.REST.httpclient;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.datafast.REST.response.RspVIP;
import com.newpos.libpay.Logger;
import com.newpos.libpay.presenter.TransUI;
import com.newpos.libpay.trans.Tcode;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

import static cn.desert.newpos.payui.master.MasterControl.callbackwaitResponseWS;

/**
 * Created by JHON MORANTES on 21/09/2016.
 */

/**
 * Esta clase realiza las peticiones al web service
 */
public class RequestWs {

    public static final String TAG = RequestWs.class.getSimpleName();
    private Context context;
    private String URL;
    private TransUI transUI;
    private int timeout;
    private int retVal = -1;

    public RequestWs(TransUI transUI, Context context, String URL, int timeout) {
        this.context = context;
        this.URL = URL;
        this.transUI = transUI;
        this.timeout = timeout;
    }

    private void callback(int codErr){
        transUI.handling(timeout, Tcode.Status.send_over_2_recv);
        if (callbackwaitResponseWS!=null) {
            callbackwaitResponseWS.getwaitResponseWS(null, null, codErr);
        }
    }

    /**
     * Este metodo realiza el llamado de la clase volley la cual se encarga del
     * realizar las peticiones al WS.
     * @param object
     * @param callback
     */
    public void HTTP_Requets(JSONObject object, final VolleyCallback callback){

        if (object == null){
            callback(Tcode.T_err_json_req);
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                URL,
                object,
                new Response.Listener<JSONObject>(){

                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccessResponse(response);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        NetworkResponse response = volleyError.networkResponse;

                        //Valida conexion a internet
                        if(volleyError instanceof NoConnectionError){
                            callback(Tcode.T_err_not_internet);
                            return;
                        }

                        if (response != null && response.data != null){
                            switch (response.statusCode){
                                case 500:
                                    retVal = Tcode.T_internal_server_err;
                                    break;
                                case 400:
                                    retVal = Tcode.T_err_bad_request;
                                    break;
                                case 404:
                                    retVal = Tcode.T_err_no_found;
                                    break;
                                default:
                                    retVal = 999;
                                    break;
                            }
                        }else{
                            retVal = Tcode.T_err_timeout;
                        }

                        callback(retVal);

                    }
                }){
            /*@Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                int statusCode = response.statusCode;

                switch (statusCode){
                    //case HttpURLConnection.HTTP_OK:
                    case HttpURLConnection.HTTP_NOT_FOUND:
                    case HttpURLConnection.HTTP_INTERNAL_ERROR:
                        //do stuff
                        //transUI.toasTrans("Error statusCode: "+statusCode, false,false);
                        break;
                }

                return super.parseNetworkResponse(response);
            }*/
        };

        //Se indica un timeout
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        MySingleton.getmInstance(context).addToRequestque(jsonObjectRequest);
    }

    /**
     * En esta interface se realiza la definicion del metodo onSuccessResponse que recibe la
     * respuesta de la peticion del metodo al WS.
     */
    public interface VolleyCallback{
        void onSuccessResponse(JSONObject result);
    }
    /**
     * realiza el parsin del objeto JSON recibido.
     * @param json
     * @return retorna el objeto con los datos JSON interpretados de la respuesta.
     */
    public RspVIP getRspVIP(JSONObject json) throws JSONException{

        RspVIP rspVIP = new RspVIP();


        if(validatedNull(json, rspVIP.T_FNC)){
            rspVIP.setFNC(json.getString(rspVIP.T_FNC));
        }

        if(validatedNull(json, rspVIP.T_TID)){
            rspVIP.setTID(json.getString(rspVIP.T_TID));
        }

        if(validatedNull(json, rspVIP.T_REF)){
            rspVIP.setREF(json.getString(rspVIP.T_REF));
        }

        if(validatedNull(json, rspVIP.T_VAL))
            rspVIP.setVAL(json.getString(rspVIP.T_VAL));

        if(validatedNull(json, rspVIP.T_MSJ)){
            rspVIP.setMSJ(json.getString(rspVIP.T_MSJ));
        }

        if(validatedNull(json, rspVIP.T_F73)){
            rspVIP.setF73(json.getString(rspVIP.T_F73));
        }

        return rspVIP;
    }

    /**
     * Valida si el objeto tiene valor null
     * @param jsonObject
     * @param key
     */
    private boolean validatedNull(JSONObject jsonObject, String key){
        return (jsonObject.has(key) && !jsonObject.isNull(key));
    }
}
