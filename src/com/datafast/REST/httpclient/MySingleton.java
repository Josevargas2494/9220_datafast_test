package com.datafast.REST.httpclient;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by jhonmorantes on 14/10/16.
 */

public class MySingleton {

    private static MySingleton mInstance;
    private RequestQueue requestQueue;
    private Context context;

    public MySingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
    }

    /*
    * @return The volley request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue(){

        if (requestQueue == null){
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static synchronized MySingleton getmInstance(Context context){

        if (mInstance == null){
            mInstance = new MySingleton(context);
        }

        return mInstance;
    }

    public <T>void addToRequestque(Request<T> request){
        requestQueue.add(request);
    }
}
