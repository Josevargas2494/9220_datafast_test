package com.datafast.REST.request;

import android.util.Log;

import com.newpos.libpay.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class ReqVIP {

    public final String NAME_SVC = "VIP_Svc_1.0";
    public final String T_FNC = "FNC";
    public final String T_MID = "MID";
    public final String T_TID = "TID";
    public final String T_LOT = "LOT";
    public final String T_REF = "REF";
    public final String T_TJT = "TJT";
    public final String T_INV = "INV";
    public final String T_LEN = "LEN";

    private String FNC;
    private String MID;
    private String TID;
    private String LOT;
    private String REF;
    private String TJT;
    private String INV;
    private String LEN;

    private int lenData;

    public ReqVIP() {
        this.lenData = 0;
    }

    public String getFNC() {
        return FNC;
    }

    public void setFNC(String FNC) {
        this.FNC = FNC;
    }

    public String getMID() {
        return MID;
    }

    public void setMID(String MID) {
        this.MID = MID;
    }

    public String getTID() {
        return TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public String getLOT() {
        return LOT;
    }

    public void setLOT(String LOT) {
        this.LOT = LOT;
    }

    public String getREF() {
        return REF;
    }

    public void setREF(String REF) {
        this.REF = REF;
    }

    public String getTJT() {
        return TJT;
    }

    public void setTJT(String TJT) {
        this.TJT = TJT;
    }

    public String getINV() {
        return INV;
    }

    public void setINV(String INV) {
        this.INV = INV;
    }

    public String getLEN() {
        return LEN;
    }

    public void setLEN(String LEN) {
        this.LEN = LEN;
    }

    public int getLenData() {
        try {
            lenData += FNC.length();
            lenData += MID.length();
            lenData += TID.length();
            lenData += LOT.length();
            lenData += REF.length();
            lenData += TJT.length();
            lenData += INV.length();
        }
        catch (Exception e){
            return 0;
        }

        return lenData;
    }

    public JSONObject buildsObjectJSON(){

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put(T_FNC, getFNC());
            jsonObject.put(T_MID, getMID());
            jsonObject.put(T_TID, getTID());
            jsonObject.put(T_LOT, getLOT());
            jsonObject.put(T_REF, getREF());
            jsonObject.put(T_TJT, getTJT());
            jsonObject.put(T_INV, getINV());
            jsonObject.put(T_LEN, getLEN());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.debug("==JSON Request VIP==");
        Logger.debug(String.valueOf(jsonObject));
        Logger.debug("====================");

        return jsonObject;
    }
}
