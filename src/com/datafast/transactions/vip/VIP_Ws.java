package com.datafast.transactions.vip;

import android.content.Context;

import com.android.desert.keyboard.InputManager;
import com.datafast.REST.httpclient.RequestWs;
import com.datafast.REST.request.ReqVIP;
import com.datafast.REST.response.RspVIP;
import com.datafast.inicializacion.prompts.Prompt;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.user.OnUserResultListener;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.presenter.TransUI;
import com.newpos.libpay.presenter.TransView;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static cn.desert.newpos.payui.master.MasterControl.callbackwaitResponseWS;
import static com.android.newpos.pay.StartAppDATAFAST.host_confi;
import static com.datafast.menus.menus.idAcquirer;

public class VIP_Ws implements TransPresenter, TransView {

    private RequestWs requestWs;
    private TransUI transUI;
    private String Pan;
    private String Field58;
    private Context context;
    private RspVIP rspVIP;
    private int timeout;
    private int timeoutWs;
    private int retVal;
    private String TransEname;
    private String idPromptVIP;
    private String TermID;
    private String MerchID;
    private String TraceNo;
    private String BatchNo;

    private String URL_VIP;
    //private String URL_VIP = "http://200.115.37.206:65126/api/POS/";//Remota
    //private String URL_VIP = "http://192.168.65.8:65126/api/POS/";//Local

    public VIP_Ws(Context ctx, String transEname, TransInputPara p, String... args) {
        context = ctx;
        transUI = p.getTransUI();
        TransEname = transEname;
        retVal = -1;
        timeout = 30 * 1000;
        Field58 = args[0];
        Pan = args[1];
        idPromptVIP = args[2];
        URL_VIP = args[3];
        TermID = args[4];
        MerchID = args[5];
        TraceNo = args[6];
        BatchNo = args[7];
    }

    @Override
    public void start() {
        if (isAeroDiners())
            packPromptAeroDiners();

        return;
    }

    @Override
    public ISO8583 getISO8583() {
        return null;
    }

    private int getTimeOutWs(){

        if (host_confi.getTIEMPO_ESPERA_RESPUESTA() != null) {
            timeoutWs = Integer.parseInt(host_confi.getTIEMPO_ESPERA_CONEXION()) * 1000;
        } else {
            timeoutWs = timeout;
        }

        return timeoutWs;
    }
    protected boolean isAeroDiners(){
        if (Field58==null) {
            return false;
        }

        if (Field58.indexOf("3731") >= 0 ||
                Field58.indexOf("3732") >= 0 ||
                Field58.indexOf("3634") >= 0)
        {
            return true;
        }

        return false;
    }

    private boolean checkURL(){
        if(!PAYUtils.isEmpty(URL_VIP))
           return true;
        else
            return false;
    }

    private String getPromt(String id){

        String tmp1;
        String tmp2 = "";
        String idPrompt;
        int len;
        int i = 0;

        while (i<=Field58.length()) {
            try {
                len = Integer.parseInt(Field58.substring(i, i + 4));
                if (len != 0) {
                    i += 4;//Len always is in 4 bytes

                    tmp1 = Field58.substring(i, i + (len * 2));

                    idPrompt = tmp1.substring(0, 4);
                    if (idPrompt.equals(id)) {
                        tmp2 = tmp1.substring(4);
                        break;
                    }
                    i += tmp1.length();
                }
            }catch (Exception e){
                tmp2 = "";
            }
        }
        return tmp2;

    }
    protected void packPromptAeroDiners() {

        if (!checkURL()){
            if (callbackwaitResponseWS!=null) {
                callbackwaitResponseWS.getwaitResponseWS(null, null, Tcode.T_url_invalid);
            }
            return;
        }else {

            requestWs = new RequestWs(transUI, context, URL_VIP, getTimeOutWs());
            rspVIP = new RspVIP();

            final ReqVIP reqVIP = new ReqVIP();
            reqVIP.setFNC(reqVIP.NAME_SVC);
            reqVIP.setMID(MerchID);
            reqVIP.setTID(TermID);
            reqVIP.setLOT(BatchNo);
            reqVIP.setREF(TraceNo);
            reqVIP.setTJT(Pan);
            String inv = ISOUtil.hex2AsciiStr(getPromt(idPromptVIP));
            reqVIP.setINV(ISOUtil.padleft(inv + "", inv.length(), '0'));
            reqVIP.setLEN(String.valueOf(reqVIP.getLenData()));

            transUI.handling(timeout, Tcode.Status.send_data_2_server);
            requestWs.HTTP_Requets(reqVIP.buildsObjectJSON(), new RequestWs.VolleyCallback() {
                @Override
                public void onSuccessResponse(JSONObject result) {

                /*Logger.debug("==JSON Response VIP==\n");
                Logger.debug(String.valueOf(result));
                Logger.debug("====================\n");*/

                    if (result == null) {
                        retVal = -1;
                        if (callbackwaitResponseWS != null) {
                            callbackwaitResponseWS.getwaitResponseWS(null, null, retVal);
                        }
                    } else {
                        try {
                            transUI.handling(timeout, Tcode.Status.send_over_2_recv);
                            rspVIP = requestWs.getRspVIP(result);
                            retVal = 0;

                            if (callbackwaitResponseWS != null) {
                                callbackwaitResponseWS.getwaitResponseWS(rspVIP.getMSJ(), rspVIP.getF73(), retVal);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            retVal = -1;
                            if (callbackwaitResponseWS != null) {
                                callbackwaitResponseWS.getwaitResponseWS(null, null, retVal);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            retVal = -1;
                            if (callbackwaitResponseWS != null) {
                                callbackwaitResponseWS.getwaitResponseWS(null, null, retVal);
                            }
                        }
                    }
                }
            });
        }
    }
    @Override
    public void showCardView(String msg, int timeout, int mode, String title, OnUserResultListener l) {

    }

    @Override
    public void showCardView(String msg, int timeout, int mode, String title) {

    }

    @Override
    public void showQRCView(int timeout, InputManager.Style mode) {

    }

    @Override
    public void showCardNo(int timeout, String pan, OnUserResultListener l) {

    }

    @Override
    public void showMessageInfo(String title, String msg, String btnCancel, String btnConfirm, int timeout, OnUserResultListener l) {

    }

    @Override
    public void showMessageImpresion(String title, String msg, String btnCancel, String btnConfirm, int timeout, OnUserResultListener l) {

    }

    @Override
    public void showInputView(int timeout, InputManager.Mode mode, OnUserResultListener l, String title) {

    }

    @Override
    public String getInput(InputManager.Mode type) {
        return null;
    }

    @Override
    public void showTransInfoView(int timeout, TransLogData data, OnUserResultListener l) {

    }

    @Override
    public void showCardAppListView(int timeout, String[] apps, OnUserResultListener l) {

    }

    @Override
    public void showMultiLangView(int timeout, String[] langs, OnUserResultListener l) {

    }

    @Override
    public void showSuccess(int timeout, String info) {

    }

    @Override
    public void showError(int timeout, String err) {

    }

    @Override
    public void showMsgInfo(int timeout, String status, boolean transaccion) {

    }

    @Override
    public void showMsgInfo(int timeout, String status, String title, boolean transaccion) {

    }

    @Override
    public void showTypeCoinView(int timeout, String title, OnUserResultListener l) {

    }

    @Override
    public void showInputUser(int timeout, String title, String label, int min, int max, OnUserResultListener l) {

    }

    @Override
    public void toasTransView(String errcode, boolean sound) {

    }

    @Override
    public void showConfirmAmountView(int timeout, String title, String label, String amnt, boolean isHTML, float textSize, OnUserResultListener l) {

    }

    @Override
    public void showCardViewImg(String img, OnUserResultListener l) {

    }

    @Override
    public void showSignatureView(int timeout, OnUserResultListener l, String title, String transType) {

    }

    @Override
    public void showListView(int timeout, OnUserResultListener l, String title, String transType, ArrayList<String> listMenu, int id) {

    }

    @Override
    public void showInputPromptView(int timeout, String transType, String nameAcq, Prompt cls, OnUserResultListener l) {

    }

    @Override
    public void getResultListener(OnUserResultListener l) {

    }
}
