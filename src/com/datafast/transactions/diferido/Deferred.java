package com.datafast.transactions.diferido;

import android.content.Context;
import android.media.ToneGenerator;

import com.android.desert.keyboard.InputInfo;
import com.android.newpos.libemv.PBOCTag9c;
import com.android.newpos.libemv.PBOCTransProperty;
import com.android.newpos.libemv.PBOCode;
import com.android.newpos.pay.R;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.transactions.common.GetAmount;
import com.newpos.bypay.EmvL2CVM;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.contactless.EmvL2Process;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.util.ArrayList;
import java.util.Objects;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.master.MasterControl;

import static cn.desert.newpos.payui.master.MasterControl.incardTable;
import static com.android.newpos.pay.StartAppDATAFAST.listPrompts;
import static com.android.newpos.pay.StartAppDATAFAST.rango;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_FALLBACK;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_SWIPE_ICC_CTL;
import static com.datafast.menus.menus.FALLBACK;
import static com.datafast.menus.menus.TOTAL_BATCH;
import static com.datafast.menus.menus.contFallback;
import static com.datafast.menus.menus.idAcquirer;
import static com.datafast.transactions.common.GetAmount.getConfirmarMontos;

public class Deferred extends FinanceTrans implements TransPresenter {

    private InputInfo inputInfo;
    private String title;

    public Deferred(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);
        para = p;
        transUI = para.getTransUI();
        isReversal = true;
        isProcPreTrans = true;
        isSaveLog = true;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
    }


    @Override
    public ISO8583 getISO8583() {
        return null;
    }


    @Override
    public void start() {

        if (!checkBatchAndSettle(true,true))
            return;

        if (!setAmount())
            return;

        if (!procesarDiferido())
            return;

        if (!CardProcess(INMODE_IC | INMODE_MAG | INMODE_NFC | INMODE_HAND))
            return;

        if(!prepareOnline())
            return;

        Logger.debug("DiferidoTrans>>finish");
        return;

    }

    private boolean procesarDiferido() {

        inputInfo = transUI.showList(timeout, TransEName, para.getTransType(), getListMenu(), R.drawable.ic_diferido);

        if (!inputInfo.isResultFlag()) {
            transUI.showError(timeout, Tcode.T_user_cancel_operation);
            return false;
        }

        TypeDeferred = inputInfo.getResult();

        if (!Cuotas()) {
            return false;
        }

        return true;
    }

    private boolean Cuotas() {
        boolean ret = false;
        while (true) {
            inputInfo = transUI.showInputUser(timeout, transEname, "INGRESE CUOTAS", 0,2);

            if (inputInfo.isResultFlag()) {
                if (inputInfo.getResult().equals("00") || inputInfo.getResult().equals("0")) {
                    transUI.toasTrans(Tcode.T_err_invalid_len, true, true);
                    continue;
                } else {
                    numCuotasDeferred = inputInfo.getResult();
                    ret = true;
                    break;
                }
            } else {
                retVal = Tcode.T_user_cancel_input;
                transUI.showError(timeout, retVal);
                break;
            }
        }
        return ret;
    }

    private ArrayList<String> getListMenu() {
        final ArrayList<String> list = new ArrayList<>();

        for (String[] str : deferredType) {
            list.add(str[1]);
        }

        return list;
    }

    /**
     * 准备联机
     */
    private boolean prepareOnline() {

        if ((retVal = CommonFunctionalities.setTipoCuenta(timeout, ProcCode, transUI, ISOUtil.stringToBoolean(rango.getTIPO_DE_CUENTA()))) != 0) {
            return false;
        }

        ProcCode = CommonFunctionalities.getProCode();

        if ((retVal = CommonFunctionalities.setPrompt(timeout, TransEName, listPrompts, transUI)) != 0) {
            return false;
        }

        Field58 = CommonFunctionalities.getFld58Prompts();

        if ((retVal = CommonFunctionalities.confirmAmount(timeout, TransEName, transUI, getConfirmarMontos())) != 0) {
            return false;
        }

        if (!requestPin()) {
            return false;
        }

        if (retVal == 0) {

            transUI.handling(timeout, Tcode.Status.connecting_center);
            setDatas(inputMode);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                retVal = OnlineTrans(emv);
            } else {
                retVal = OnlineTrans(null);
            }

            Logger.debug("Diferido>>OnlineTrans=" + retVal);
            clearPan();
            if (retVal == 0) {
                msgAprob(Tcode.Status.diferido_exitoso,true);
                return true;
            }else {
                transUI.showError(timeout, retVal);
                return false;
            }

        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
    }
}
