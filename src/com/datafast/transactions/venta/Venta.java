package com.datafast.transactions.venta;

import android.content.Context;
import com.datafast.transactions.callbacks.waitResponseWS;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.transactions.vip.VIP_Ws;
import com.newpos.libpay.Logger;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import cn.desert.newpos.payui.master.MasterControl;

import static com.android.newpos.pay.StartAppDATAFAST.listPrompts;
import static com.android.newpos.pay.StartAppDATAFAST.rango;
import static com.android.newpos.pay.StartAppDATAFAST.tconf;
import static com.datafast.menus.menus.idAcquirer;
import static com.datafast.transactions.common.GetAmount.getConfirmarMontos;

public class Venta extends FinanceTrans implements TransPresenter {

    /**
     * 金融交易类构造
     *
     * @param ctx        Context
     * @param transEname Nombre Transaccion
     * @param p          Parametros
     */
    public Venta(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);
        init(transEname, p);
    }

    private void init(String transEname, TransInputPara p) {
        para = p;
        transUI = para.getTransUI();
        isReversal = true;
        isProcPreTrans = true;
        isSaveLog = true;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
    }

    @Override
    public ISO8583 getISO8583() {
        return null;
    }

    @Override
    public void start() {

        if (!checkBatchAndSettle(true,true))
            return;

        if (!setAmount())
            return;

        if (!CardProcess(INMODE_IC | INMODE_MAG | INMODE_NFC | INMODE_HAND))
            return;

        if(!prepareOnline())
            return;

        Logger.debug("SaleTrans>>finish");
        return;
    }

    private void VIP(waitResponseWS call) {
        String promptSelect = "3731";
        MasterControl.callbackwaitResponseWS = call;
        TransInputPara para = new TransInputPara();
        para.setTransUI(transUI);
        para.setTransType(Type.VENTA);
        para.setNeedOnline(false);
        para.setNeedPrint(false);
        para.setNeedConfirmCard(false);
        para.setNeedPass(false);
        para.setNeedAmount(false);
        para.setEmvAll(false);
        final TransPresenter presenter = new VIP_Ws(this.context,
                Type.VENTA,
                para,
                Field58,
                Pan,
                promptSelect,
                tconf.getHEADER_DIRECCION_2(),
                PAYUtils.replace(TermID," ", ""),
                PAYUtils.replace(MerchID," ", ""),
                PAYUtils.replace(TraceNo," ", ""),
                PAYUtils.replace(BatchNo," ", ""));

        new Thread() {
            @Override
            public void run() {
                presenter.start();
            }
        }.start();

    }
    /**
     * 准备联机
     */
    private boolean prepareOnline() {

        if (ISOUtil.stringToBoolean(tconf.getHABILITA_MONTO_FIJO())){
            amount.sumarMontoFijo();
            montoFijo = amount.getmontoFijo();
            tipoMontoFijo = amount.getTipoMontoFijo();
        }

        if ((retVal = CommonFunctionalities.setTipoCuenta(timeout, ProcCode, transUI, ISOUtil.stringToBoolean(rango.getTIPO_DE_CUENTA()))) != 0) {
            return false;
        }

        ProcCode = CommonFunctionalities.getProCode();

        if ((retVal = CommonFunctionalities.setPrompt(timeout, TransEName, listPrompts, transUI)) != 0) {
            return false;
        }

        Field58 = CommonFunctionalities.getFld58Prompts();

        if ((retVal = CommonFunctionalities.confirmAmount(timeout, TransEName, transUI, getConfirmarMontos())) != 0) {
            return false;
        }

        if (!requestPin()) {
            return false;
        }

        if (retVal == 0) {

            transUI.handling(timeout, Tcode.Status.connecting_center);
            setDatas(inputMode);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                if(isAeroDiners()){
                    SALAVIP();
                }else {
                    retVal = OnlineTrans(emv);
                    endTrans();
                }
            } else {
                if(isAeroDiners()){
                    SALAVIP();
                }else {
                    retVal = OnlineTrans(null);
                    endTrans();
                }
            }
        } else {
            transUI.showError(timeout, retVal);
            clearPan();
            return false;
        }
        return false;
    }

    private boolean endTrans(){
        Logger.debug("SaleTrans>>OnlineTrans=" + retVal);
        if (retVal == 0) {
            //Solo se usa en la venta (Gasolinera)
            CommonFunctionalities.obtenerBin(Pan);
            msgAprob(Tcode.Status.sale_succ,true);
            clearPan();
            return true;
        }else {
            transUI.showError(timeout, retVal);
            clearPan();
            return false;
        }
    }

    synchronized private void SALAVIP(){
        VIP(new waitResponseWS() {
            @Override
            public void getwaitResponseWS(final String Msg, final String f73, int status) {
                retVal = status;
                if (status==0) {
                    /*Logger.debug("------Msg------\n");
                    Logger.debug(Msg);
                    Logger.debug("---------------\n");
                    Logger.debug("------f73------\n");
                    Logger.debug(f73);
                    Logger.debug("---------------\n");*/

                    new Thread() {
                        @Override
                        public void run() {
                            if(parsingWsVIP(Msg,f73)==0) {
                                retVal = OnlineTrans(emv);
                            }
                            if (retVal != 0) {
                                if (isTraceNoInc) {
                                    cfg.incTraceNo();
                                }
                            }
                            endTrans();
                        }
                    }.start();

                }else {
                    //Por solicitud de diners
                    if (isTraceNoInc) {
                        cfg.incTraceNo();
                    }
                    endTrans();
                }
            }
        });
    }
}
