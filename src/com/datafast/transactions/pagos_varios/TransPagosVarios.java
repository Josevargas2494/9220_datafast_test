package com.datafast.transactions.pagos_varios;

import android.content.Context;
import android.media.ToneGenerator;

import com.android.desert.keyboard.InputInfo;
import com.android.newpos.libemv.PBOCTag9c;
import com.android.newpos.libemv.PBOCTransProperty;
import com.android.newpos.libemv.PBOCode;
import com.android.newpos.pay.R;
import com.datafast.inicializacion.pagosvarios.GrupoPagosVarios;
import com.datafast.inicializacion.pagosvarios.PagosVarios;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.transactions.common.GetAmount;
import com.newpos.bypay.EmvL2CVM;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.contactless.EmvL2Process;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.master.MasterControl;

import static cn.desert.newpos.payui.master.MasterControl.incardTable;
import static com.android.newpos.pay.StartAppDATAFAST.listPagosVarios;
import static com.android.newpos.pay.StartAppDATAFAST.listPrompts;
import static com.android.newpos.pay.StartAppDATAFAST.rango;
import static com.android.newpos.pay.StartAppDATAFAST.tconf;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_FALLBACK;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_SWIPE_ICC_CTL;
import static com.datafast.menus.menus.FALLBACK;
import static com.datafast.menus.menus.TOTAL_BATCH;
import static com.datafast.menus.menus.contFallback;
import static com.datafast.menus.menus.idAcquirer;
import static com.datafast.transactions.common.GetAmount.getConfirmarMontos;

public class TransPagosVarios extends FinanceTrans implements TransPresenter {

    private InputInfo inputInfo;
    private String title;
    private int index = 0;

    public TransPagosVarios(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);
        para = p;
        transUI = para.getTransUI();
        isReversal = true;
        isProcPreTrans = true;
        isSaveLog = true;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
    }

    @Override
    public ISO8583 getISO8583() {
        return null;
    }

    @Override
    public void start() {

        if (!checkBatchAndSettle(true,true))
            return;

        if (procesarPagosVarios())
            return;

        return;
    }

    private boolean procesarPagosVarios(){
        title = "PAGOS VARIOS";

        llenarListPagosVarios();

        if (listPagosVarios == null || listPagosVarios.isEmpty()) {
            transUI.showError(timeout, Tcode.T_not_list);
            return false;
        }

        inputInfo = transUI.showList(timeout, title, Type.PAGOS_VARIOS, getListMenu(), R.drawable.ic_pagosvarios);
        if (!inputInfo.isResultFlag()) {
            transUI.showError(timeout, Tcode.T_user_cancel_operation);
            return false;
        }

        procesarSeleccion(inputInfo.getResult());

        if (index >= 0){

            PagosVarios.PV_GRUPOPROMPT = listPagosVarios.get(index).getGRUPO_PROMPTS();
            pagoVarioSeleccionado = listPagosVarios.get(index).getCODIGO_PAGOS_VARIOS();
            pagoVarioSeleccionadoNombre = listPagosVarios.get(index).getNOMBRE_PAGOS_VARIOS();

            if (!setAmount())
                return false;

            if (!CardProcess(INMODE_IC | INMODE_MAG | INMODE_NFC | INMODE_HAND))
                return false;

            if(!prepareOnline())
                return false;

            return true;

        }else{
            transUI.showError(timeout, Tcode.T_not_allow);
            return false;
        }
    }

    private int procesarSeleccion(String seleccion){
        Iterator<PagosVarios> itrPagosVarios = listPagosVarios.iterator();
        while (itrPagosVarios.hasNext()){
            PagosVarios pagosVariosActual = itrPagosVarios.next();
            if (seleccion.equals(pagosVariosActual.getTEXTO_PAGOS_VARIOS())){
                break;
            }
            index++;
        }

        return index;
    }

    private ArrayList<String> getListMenu() {
        final ArrayList<String> list = new ArrayList<>();
        Iterator<PagosVarios> itrPagosVarios = listPagosVarios.iterator();

        while (itrPagosVarios.hasNext()){
            PagosVarios pagosVariosActual = itrPagosVarios.next();
            list.add(pagosVariosActual.getTEXTO_PAGOS_VARIOS());
        }
        return list;
    }

    private void llenarListPagosVarios(){
        listPagosVarios = new ArrayList<>();
        listPagosVarios = GrupoPagosVarios.GetListaPagosVarios(tconf.getGRUPO_PAGOS_VARIOS(), context);
        if (listPagosVarios == null){
            listPagosVarios = new ArrayList<>();
            listPagosVarios.clear();
        }else  if (listPagosVarios.isEmpty())
            listPagosVarios.clear();
    }

    /**
     * 准备联机
     */
    private boolean prepareOnline() {

        if ((retVal = CommonFunctionalities.setTipoCuenta(timeout, ProcCode, transUI, ISOUtil.stringToBoolean(rango.getTIPO_DE_CUENTA()))) != 0) {
            return false;
        }

        ProcCode = CommonFunctionalities.getProCode();

        if ((retVal = CommonFunctionalities.setPrompt(timeout, TransEName, listPrompts, transUI)) != 0) {
            retVal = Tcode.T_user_cancel_input;
            return false;
        }

        Field58 = CommonFunctionalities.getFld58Prompts();

        if ((retVal = CommonFunctionalities.confirmAmount(timeout, TransEName, transUI, getConfirmarMontos())) != 0) {
            retVal = Tcode.T_user_cancel_input;
            return false;
        }

        if (!requestPin()) {
            return false;
        }

        if (retVal == 0) {

            transUI.handling(timeout, Tcode.Status.connecting_center);
            setDatas(inputMode);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                retVal = OnlineTrans(emv);
            } else {
                retVal = OnlineTrans(null);
            }
            Logger.debug("PagosVarios>>OnlineTrans=" + retVal);
            clearPan();
            if (retVal == 0) {
                msgAprob(Tcode.Status.pago_vario_succ,true);
                return true;
            }else {
                transUI.showError(timeout, retVal);
                return false;
            }
        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
    }
}
