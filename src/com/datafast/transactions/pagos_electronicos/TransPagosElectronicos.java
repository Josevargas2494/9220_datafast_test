package com.datafast.transactions.pagos_electronicos;

import android.content.Context;
import android.media.ToneGenerator;

import com.android.desert.keyboard.InputInfo;
import com.android.newpos.pay.R;
import com.datafast.inicializacion.pagoselectronicos.GrupoPagosElectronicos;
import com.datafast.inicializacion.pagoselectronicos.PagosElectronicos;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.transactions.common.GetAmount;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.contactless.EmvL2Process;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.util.ArrayList;
import java.util.Iterator;

import cn.desert.newpos.payui.UIUtils;

import static cn.desert.newpos.payui.master.MasterControl.incardTable;
import static com.android.newpos.pay.StartAppDATAFAST.listPrompts;
import static com.android.newpos.pay.StartAppDATAFAST.tconf;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_TOKEN_PE;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.ITEM_PAGOS_ELECTRONICOS;
import static com.datafast.menus.menus.TOTAL_BATCH;
import static com.datafast.menus.menus.idAcquirer;
import static com.datafast.transactions.common.GetAmount.getConfirmarMontos;

public class TransPagosElectronicos extends FinanceTrans implements TransPresenter {

    private InputInfo inputInfo;
    private String title = "PAGOS CON CODIGO";
    public static String tipoPagoElectronico;
    private ArrayList<PagosElectronicos> listPagoElectronico;
    private int index = 0;

    public TransPagosElectronicos(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);

        para = p;
        transUI = para.getTransUI();
        isReversal = true;
        isProcPreTrans = true;
        isSaveLog = true;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
        ExpDate = "0000";
        inputMode = ENTRY_MODE_HAND;
    }

    public static String getTipoPagoElectronico() {
        return tipoPagoElectronico;
    }

    public static void setTipoPagoElectronico(String tipoPagoElectronico) {
        TransPagosElectronicos.tipoPagoElectronico = tipoPagoElectronico;
    }

    @Override
    public ISO8583 getISO8583() {
        return null;
    }

    @Override
    public void start() {

        if (!checkBatchAndSettle(true,true))
            return;

        if (!procesarPagoElectronico())
           return;

        Logger.debug("PagoElecTrans>>finish");
        return;
    }

    private boolean procesarPagoElectronico() {
        String typeInput;

        llenarListPagosElectronico();

        if (listPagoElectronico == null || listPagoElectronico.isEmpty()) {
            transUI.showError(timeout, Tcode.T_not_list_pe);
            return false;
        }

        inputInfo = transUI.showList(timeout, title, Trans.Type.ELECTRONIC, getListMenuDinamic(), R.drawable.ic_pagoselectronicos);
        if (!inputInfo.isResultFlag()) {
            transUI.showError(timeout, Tcode.T_user_cancel_operation);
            return false;
        }

        TypeTransElectronic = inputInfo.getResult();

        procesarSeleccion(TypeTransElectronic);

        if (index >= 0){

            if (TypeTransElectronic.equals(listPagoElectronico.get(index).getNOMBRE_PAGO_ELECTRONICO())) {
                transUI.showCardImg(listPagoElectronico.get(index).getIMAGEN());
                if (listPagoElectronico.get(index).getNOMBRE_PAGO_ELECTRONICO().equals(Type.PAYCLUB))
                    TypeTransElectronic = Type.PAYCLUB;
                else if (listPagoElectronico.get(index).getNOMBRE_PAGO_ELECTRONICO().equals(Type.PAYBLUE))
                    TypeTransElectronic = Type.PAYBLUE;
                else{
                    transUI.showError(timeout, Tcode.T_not_allow);
                    return false;
                }

                TypeTransElectronic = listPagoElectronico.get(index).getNOMBRE_PAGO_ELECTRONICO();
                setTipoPagoElectronico(TypeTransElectronic);
                title = TypeTransElectronic;
            }

            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }

            transUI = para.getTransUI();
            inputInfo = transUI.showList(timeout, title, Trans.Type.ELECTRONIC, getListMenuAll(), R.drawable.ic_pe);
            if (!inputInfo.isResultFlag()) {
                transUI.showError(timeout, Tcode.T_user_cancel_operation);
                return false;
            }

            typeInput = inputInfo.getResult();

            if (typeInput.equals("VENTA")) {
                if (!processWallet()) {
                    return false;
                }
            }

            if (typeInput.equals("DIFERIDOS")) {
                para.setTransType(Type.ELECTRONIC_DEFERRED);
                TransEName = Type.ELECTRONIC_DEFERRED;

                inputInfo = transUI.showList(timeout, title, para.getTransType(), getListMenuType(), R.drawable.ic_pe);

                if (!inputInfo.isResultFlag()) {
                    transUI.showError(timeout, Tcode.T_user_cancel_operation);
                    return false;
                }

                TypeDeferred = inputInfo.getResult();

                if (!Cuotas()) {
                    return false;
                } else {
                    if (!processWallet()) {
                        return false;
                    }
                }
            }

            if (!incardTable(Pan, TransEName,TypeTransElectronic)) {
                transUI.showError(timeout, Tcode.T_unsupport_card);
                return false;
            }

            if (!setAmount())
                return false;

            if (!prepareOnline())
                return false;

            return true;

        }else{
            transUI.showError(timeout, Tcode.T_not_allow);
            return false;
        }
    }

    private ArrayList<String> getListMenuType() {
        final ArrayList<String> list = new ArrayList<>();

        for (String[] str : deferredType) {
            list.add(str[1]);
        }
        return list;
    }

    private void llenarListPagosElectronico(){
        listPagoElectronico = new ArrayList<>();
        listPagoElectronico = GrupoPagosElectronicos.GetListaPagosElectronicos(tconf.getGRUPO_PAGOS_ELECTRONICOS(), context);
        if (listPagoElectronico == null){
            listPagoElectronico = new ArrayList<>();
            listPagoElectronico.clear();
        }else  if (listPagoElectronico.isEmpty())
            listPagoElectronico.clear();
    }

    private ArrayList<String> getListMenuDinamic() {
        final ArrayList<String> list = new ArrayList<>();
        Iterator<PagosElectronicos> itrPagosElect = listPagoElectronico.iterator();

        while (itrPagosElect.hasNext()){
            PagosElectronicos pagosElectActual = itrPagosElect.next();
            list.add(pagosElectActual.getNOMBRE_PAGO_ELECTRONICO());
        }
        return list;
    }

    private int procesarSeleccion(String seleccion){
        Iterator<PagosElectronicos> itrPagosElectronicos = listPagoElectronico.iterator();
        while (itrPagosElectronicos.hasNext()){
            PagosElectronicos pagosElectActual = itrPagosElectronicos.next();
            if (seleccion.equals(pagosElectActual.getNOMBRE_PAGO_ELECTRONICO())){
                break;
            }
            index++;
        }

        return index;
    }

    private ArrayList<String> getListMenu() {
        final ArrayList<String> list = new ArrayList<>();
        list.add("1. PAY CLUB");
        list.add("2. BDP WALLET");
        return list;
    }

    private ArrayList<String> getListMenuAll() {
        final ArrayList<String> list = new ArrayList<>();
        list.add("VENTA");
        list.add("DIFERIDOS");
        return list;
    }

    private int getCodeOTT() {
        inputInfo = transUI.showInputUser(timeout, title, "CODIGO OTT", Integer.parseInt(listPagoElectronico.get(index).getLONGITUD_MINIMA()), Integer.parseInt(listPagoElectronico.get(index).getLONGITUD_MAXIMA()));

        if (!inputInfo.isResultFlag()) {
            retVal = Tcode.T_user_cancel_operation;
            transUI.showError(timeout, retVal);
            return retVal;
        }else {
            Pan = listPagoElectronico.get(index).getNUM_TARJETA();
            Pan += inputInfo.getResult();
            CodOTT = inputInfo.getResult();
            retVal = 0;
        }

        return  retVal;
    }

    private boolean processWallet() {

        CardInfo cardInfo = transUI.getCardUse(GERCARD_MSG_TOKEN_PE, timeout, INMODE_NFC | INMODE_HAND, transEname);

        if (cardInfo.isResultFalg()) {
            int type = cardInfo.getCardType();
            switch (type) {
                case CardManager.TYPE_NFC:
                    inputMode = ENTRY_MODE_NFC;
                    break;
                case CardManager.TYPE_HAND:
                    inputMode = ENTRY_MODE_HAND;
                    break;
                default:
                    transUI.showError(timeout, Tcode.T_not_allow);
                    return false;
            }
            para.setInputMode(inputMode);
            if (inputMode == ENTRY_MODE_NFC) {
                if(PBOCTrans())
                    return true;
            }
            if (inputMode == ENTRY_MODE_HAND) {
                isDebit = false;
                if(isHandle())
                    return true;
            }
        } else {
            retVal = cardInfo.getErrno();
            transUI.showError(timeout, retVal);
            return false;
        }
        return false;
    }

    private boolean PBOCTrans() {

        int code = 0;
        transUI.handling(timeout, Tcode.Status.process_trans);

        emvl2 = new EmvL2Process(this.context, para);
        emvl2.setTraceNo(TraceNo);//JM
        emvl2.setTypeTrans(TransEName);

        if ((retVal = emvl2.emvl2ParamInit()) != 0) {
            switch (retVal) {
                case 1:
                    retVal = Tcode.T_err_not_file_terminal;
                    break;
                case 2:
                    retVal = Tcode.T_err_not_file_processing;
                    break;
                case 3:
                    retVal = Tcode.T_err_not_file_entry_point;
                    break;
            }
            transUI.showError(timeout, retVal);
            return false;
        }

        emvl2.SetAmount(Amount, 0);
        emvl2.setTypeCoin(typeCoin);//JM
        code = emvl2.start();

        Logger.debug("EmvL2Process return = " + code);
        if ((code != 0) ||  (emvl2.tkn == null)) {
            transUI.showError(timeout, Tcode.T_err_detect_card_failed);
            return false;
        }

        Pan = listPagoElectronico.get(index).getNUM_TARJETA();
        Pan += emvl2.tkn;

        //CodOTT = emvl2.tkn;
        if (TypeTransElectronic.equals(Type.PAYCLUB))
            CodOTT = emvl2.tkn;
        else
            TokenElectronic = emvl2.tkn;

        return true;
    }

    private boolean isHandle() {
        if (TypeTransElectronic.equals(Type.PAYCLUB))
            inputInfo = transUI.showInputUser(timeout, title, "CODIGO OTT", Integer.parseInt(listPagoElectronico.get(index).getLONGITUD_MINIMA()), Integer.parseInt(listPagoElectronico.get(index).getLONGITUD_MAXIMA()));
        else
            inputInfo = transUI.showInputUser(timeout, title, "TOKEN", Integer.parseInt(listPagoElectronico.get(index).getLONGITUD_MINIMA()), Integer.parseInt(listPagoElectronico.get(index).getLONGITUD_MAXIMA()));

        if (!inputInfo.isResultFlag()) {
            transUI.showError(timeout, Tcode.T_user_cancel_operation);
            return false;
        }else {
            Pan = listPagoElectronico.get(index).getNUM_TARJETA();
            Pan += inputInfo.getResult();
            if (TypeTransElectronic.equals(Type.PAYCLUB))
                CodOTT = inputInfo.getResult();
            else
                TokenElectronic = inputInfo.getResult();

            return true;
        }
    }

    private boolean Cuotas() {
        boolean ret = false;
        while (true) {
            inputInfo = transUI.showInputUser(timeout, transEname, "INGRESE CUOTAS", 0,2);

            if (inputInfo.isResultFlag()) {
                if (inputInfo.getResult().equals("00") || inputInfo.getResult().equals("0")) {
                    transUI.toasTrans(Tcode.T_err_invalid_len, true, true);
                    continue;
                } else {
                    numCuotasDeferred = inputInfo.getResult();
                    ret = true;
                    break;
                }
            } else {
                transUI.showError(timeout, Tcode.T_user_cancel_input);
                break;
            }
        }
        return ret;
    }

    /**
     * 准备联机
     */
    private boolean prepareOnline() {

        if ((retVal = CommonFunctionalities.setPrompt(timeout, TransEName, ITEM_PAGOS_ELECTRONICOS, listPrompts, transUI)) != 0) {
            return false;
        }

        Field58 = CommonFunctionalities.getFld58Prompts();

        if ((retVal = CommonFunctionalities.confirmAmount(timeout, TransEName, ITEM_PAGOS_ELECTRONICOS, transUI, getConfirmarMontos())) != 0) {
            return false;
        }

        if (retVal == 0) {

            transUI.handling(timeout, Tcode.Status.connecting_center);
            setDatas(INMODE_HAND);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                retVal = OnlineTrans(emv);
            } else {
                retVal = OnlineTrans(null);
            }
            Logger.debug("PagosCodigo>>OnlineTrans=" + retVal);
            clearPan();
            if (retVal == 0) {
                msgAprob(Tcode.Status.pago_electronico_exitoso,true);
                return true;
            }else {
                transUI.showError(timeout, retVal);
                return false;
            }

        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
    }
}
