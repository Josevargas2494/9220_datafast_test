package com.datafast.transactions.pre_voucher;

import android.content.Context;
import android.media.ToneGenerator;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputManager;
import com.android.newpos.libemv.PBOCTag9c;
import com.android.newpos.libemv.PBOCTransProperty;
import com.android.newpos.libemv.PBOCode;
import com.android.newpos.pay.StartAppDATAFAST;
import com.datafast.tools_bacth.ToolsBatch;
import com.datafast.transactions.common.CommonFunctionalities;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.contactless.EmvL2Process;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.process.QpbocTransaction;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;
import com.pos.device.emv.EMVHandler;
import com.pos.device.emv.IEMVHandler;

import cn.desert.newpos.payui.UIUtils;

import static cn.desert.newpos.payui.master.MasterControl.incardTable;
import static com.android.newpos.pay.StartAppDATAFAST.listPrompts;
import static com.android.newpos.pay.StartAppDATAFAST.rango;
import static com.android.newpos.pay.StartAppDATAFAST.tconf;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.FILE_NAME_PREVOUCHER;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_CTL;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_ICC;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_SWIPE;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.TITULO_ANULACION;
import static com.datafast.menus.menus.idAcquirer;
import static com.newpos.libpay.trans.Tcode.T_void_card_not_same;

public class PagoPreVoucher extends FinanceTrans implements TransPresenter {

    private TransLogData data;
    private InputInfo info;

    public PagoPreVoucher(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);
        init(transEname, p);
    }

    private void init(String transEname, TransInputPara p) {
        para = p;
        transUI = para.getTransUI();
        isReversal = true;
        isProcPreTrans = true;
        isSaveLog = true;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
    }

    @Override
    public ISO8583 getISO8583() {
        return iso8583;
    }

    @Override
    public void start() {

        if(!checkBatchAndSettle(false,true))
            return;

        if (!haveTrans())
            return;

        if (!requestTracer())
            return;

        if(!searchPreVoucher())
            return;

        Logger.debug("PagoPreVoucher>>finish");
        return;
    }

    /**
     * Proceso de busqueda del PreVoucher
     */
    private boolean searchPreVoucher() {
        TransLog log = TransLog.getInstance(idLote + FILE_NAME_PREVOUCHER);
        data = log.searchTransLogByTraceNo(info.getResult());

        if (data != null && !data.getIsVoided()) {
            if(processPay())
                return true;
        } else {
            processErrPay();
        }
        return false;
    }

    /**
     * Proceso de Pago
     */
    private boolean processPay() {
        retVal = transUI.showTransInfo(timeout, data);

        if (0 == retVal) {

            isFallBack = data.isFallback();

            if(!setFieldsPago())
                return false;

            if (!data.isFallback()) {
                CardInfo cardInfo = null;
                if (data.getEntryMode().equals(MODE_MAG + CapPinPOS())) {
                    cardInfo = transUI.getCardUse(GERCARD_MSG_SWIPE, timeout, INMODE_MAG, transEname);
                } else if (data.getEntryMode().equals(MODE_ICC + CapPinPOS())) {
                    cardInfo = transUI.getCardUse(GERCARD_MSG_ICC, timeout, INMODE_IC, transEname);
                } else if (data.getEntryMode().equals(MODE_CTL + CapPinPOS())) {
                    cardInfo = transUI.getCardUse(GERCARD_MSG_CTL, timeout, INMODE_NFC, transEname);
                } else if (data.getEntryMode().equals(MODE_HANDLE + CapPinPOS())) {
                    if(isHandlePagoPrevoucher())
                        return true;
                    else
                        return false;
                } else {
                    transUI.showError(timeout, Tcode.T_unknow_err);
                    return false;
                }

                if (cardInfo != null) {
                    if (afterGetCardUse(cardInfo))
                        return true;
                }
                else {
                    transUI.showError(timeout, Tcode.T_user_cancel_operation);
                    return false;
                }

            } else {

                Pan = data.getPanNormal();
                ExpDate = data.getExpDate();

                if (!incardTable(Pan, TransEName)) {
                    transUI.showError(timeout, Tcode.T_unsupport_card);
                    return false;
                }

                if (data.getPanNormal().equals(Pan)) {
                    //CommonFunctionalities.checkExpDate(data.getPanNormal(), ISOUtil.stringToBoolean(rango.getFECHA_EXP()));
                    if(prepareOnline())
                        return true;
                } else {
                    transUI.showError(timeout, T_void_card_not_same);
                    return false;
                }
            }
        } else {
            transUI.showError(timeout, Tcode.T_user_cancel_operation);
            return false;
        }
        return false;
    }

    /**
     * Procesa e indica el error cuando no es permitido el pago de PreVoucher
     */
    private boolean processErrPay() {
        if (data != null) {
            if (data.getIsVoided()) {
                transUI.showError(timeout, Tcode.T_err_prevoucher_already_paid);
                return false;
            }else{
                return false;
            }
        } else {
            transUI.showError(timeout, Tcode.T_not_find_trans);
            return false;
        }
    }

    /**
     * Verifica si el comercio tiene PreVoucher en su lote
     */
    private boolean haveTrans() {
        if (!ToolsBatch.statusTrans(idLote + FILE_NAME_PREVOUCHER)) {
            transUI.showError(timeout, Tcode.T_err_no_trans);
            return false;
        }

        return true;
    }

    /**
     * Solicita el numero de referencia del PreVoucher que se quiere pagar
     */
    private boolean requestTracer() {

        info = transUI.getOutsideInput(timeout, InputManager.Mode.VOUCHER, TITULO_ANULACION);

        if (info.isResultFlag())
            return true;
        else {
            transUI.showError(timeout, Tcode.T_user_cancel_input);
            return false;
        }
    }

    private boolean setFieldsPago() {

        if (data.getProcCode() != null) {
            ProcCode = ProcCode.substring(0, 2) + data.getProcCode().substring(2);
        }
        if (data.getTraceNo() != null)//DE11
            TraceNo = data.getTraceNo();

        if (data.getLocalTime() != null)
            LocalTime = data.getLocalTime();//DE12

        if (data.getLocalDate() != null)
            LocalDate = data.getLocalDate();//DE13

        if (data.getEntryMode() != null)
            EntryMode = data.getEntryMode();//DE22

        if (data.getTypeCoin() != null)
            typeCoin = data.getTypeCoin();// tipo de moneda de la transaccion

        AmountBase0 = data.getAmmount0();
        AmountXX = data.getAmmountXX();
        IvaAmount = data.getAmmountIVA();

        if (data.isTip()){
            if (data.getTransEName().equals(Type.PREVOUCHER)){
                if(!setPropinaPagoPrevoucher())
                    return false;
            }
        }else {
            TipAmount = data.getTipAmout();
        }
        ServiceAmount = data.getAmmountService();
        Amount = data.getAmount();//DE4

        if (EntryMode == MODE_ICC)
            setICCData();
        else if (EntryMode == MODE_CTL) {
            setICCDataCTL();
        }

        if (data.getNii() != null)
            Nii = data.getNii();//DE24

        if (data.getSvrCode() != null)
            SvrCode = data.getSvrCode();//DE25

        if (!isFallBack) {
            if (data.getTrack2() != null)
                Track2 = data.getTrack2();//DE35
        }

        if (data.getRRN() != null)
            RRN = data.getRRN();//DE37

        if (data.getTermID() != null)
            TermID = data.getTermID();//DE41

        if (data.getMerchID() != null)
            MerchID = data.getMerchID();//DE42

        if (data.getCurrencyCode() != null)
            CurrencyCode = data.getCurrencyCode();//DE49

        if (data.getPIN() != null)
            PIN = data.getPIN();//DE52

        if (data.getTraceNo() != null)
            Field62 = data.getTraceNo();//DE62

        if (data.getField63() != null)
            Field63 = data.getField63();//DE63

        if (data.getPanNormal() != null)
            Pan = data.getPanNormal();

        if (data.getEName() != null)
            TypeTransVoid = data.getEName();

        if (data.getExpDate() != null)
            ExpDate = data.getExpDate();

        return true;
    }

    private boolean afterGetCardUse(CardInfo info) {
        if (info.isResultFalg()) {
            int type = info.getCardType();
            switch (type) {
                case CardManager.TYPE_MAG:
                    inputMode = ENTRY_MODE_MAG;
                    break;
                case CardManager.TYPE_ICC:
                    inputMode = ENTRY_MODE_ICC;
                    break;
                case CardManager.TYPE_NFC:
                    inputMode = ENTRY_MODE_NFC;
                    break;
            }
            para.setInputMode(inputMode);
            if (inputMode == ENTRY_MODE_MAG) {
                if(isMag(info.getTrackNo()))
                    return true;
            }
            if (inputMode == ENTRY_MODE_ICC) {
                if(isICC())
                    return true;
            }
            if (inputMode == ENTRY_MODE_NFC) {
                if(PBOCTrans())
                    return true;
            }
        } else {
            transUI.showError(timeout, info.getErrno());
            return false;
        }
        return false;
    }

    private boolean isICC() {
        String creditCard = "SI";

        transUI.handling(timeout, Tcode.Status.handling);
        emv = new EmvTransaction(para, Type.PAGO_PRE_VOUCHER);
        emv.setTraceNo(TraceNo);
        retVal = emv.start();

        if (1 == retVal || retVal == 0) {
            //Credito
            if (PAYUtils.isNullWithTrim(emv.getPinBlock())) {
                isPinExist = true;
            }//Cancelo usuario
            else if (emv.getPinBlock().equals("CANCEL")) {
                isPinExist = false;
            }//debito
            else {
                creditCard = "NO";
                isPinExist = true;
            }
            if (isPinExist) {
                if (creditCard.equals("NO"))
                    PIN = emv.getPinBlock();

                setICCData();
                if (data.getPanNormal().equals(Pan)) {
                    if(prepareOnline())
                        return true;
                } else {
                    transUI.showError(timeout, T_void_card_not_same);
                    return false;
                }
            } else {
                transUI.showError(timeout, Tcode.T_user_cancel_pin_err);
                return false;
            }

        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
        return false;
    }

    private boolean isMag(String[] tracks) {
        String data1 = null;
        String data2 = null;
        String data3 = null;
        int msgLen = 0;
        if (tracks[0].length() > 0 && tracks[0].length() <= 80) {
            data1 = new String(tracks[0]);
        }
        if (tracks[1].length() >= 13 && tracks[1].length() <= 37) {
            data2 = new String(tracks[1]);
            if (!data2.contains("=")) {
                retVal = Tcode.T_search_card_err;
            } else {
                String judge = data2.substring(0, data2.indexOf('='));
                if (judge.length() < 13 || judge.length() > 19) {
                    retVal = Tcode.T_search_card_err;
                } else {
                    if (data2.indexOf('=') != -1) {
                        msgLen++;
                    }
                }
            }
        }
        if (tracks[2].length() >= 15 && tracks[2].length() <= 107) {
            data3 = new String(tracks[2]);
        }
        if (retVal != 0) {
            transUI.showError(timeout, retVal);
            return false;
        } else {
            if (msgLen == 0) {
                transUI.showError(timeout, Tcode.T_search_card_err);
                return false;
            } else {

                try {
                    if (!incardTable(data2.substring(0, data2.indexOf('=')), TransEName)) {
                        transUI.showError(timeout, Tcode.T_unsupport_card);
                        return false;
                    }
                }catch (IndexOutOfBoundsException e) {
                    transUI.showError(timeout, Tcode.T_read_app_data_err);
                    return false;
                }

                int splitIndex = data2.indexOf("=");

                if (ISOUtil.stringToBoolean(rango.getPIN_SERVICE_CODE())) {
                    char isDebitChar = data2.charAt(splitIndex + 7);
                    if (isDebitChar == '0' || isDebitChar == '5' || isDebitChar == '6' || isDebitChar == '7') {
                        isDebit = true;
                    }
                }

                if (!ISOUtil.stringToBoolean(rango.getOMITIR_EMV())) {
                    if (data2.length() - splitIndex >= 5) {
                        char iccChar = data2.charAt(splitIndex + 5);

                        if ((iccChar == '2' || iccChar == '6') && (!isFallBack)) {
                            transUI.showError(timeout, Tcode.T_ic_not_allow_swipe);
                            return false;
                        } else {
                            if(afterMAGJudge(data1, data2, data3))
                                return true;
                        }
                    } else {
                        transUI.showError(timeout, Tcode.T_search_card_err);
                        return false;
                    }
                } else {
                    if(afterMAGJudge(data1, data2, data3))
                        return true;
                }
            }
        }
        return false;
    }

    private boolean afterMAGJudge(String data1, String data2, String data3) {
        String cardNo = data2.substring(0, data2.indexOf('='));
        Pan = cardNo;
        Track1 = data1;
        Track2 = data2;
        Track3 = data3;

        if (data.getPanNormal().equals(Pan)) {
            if (prepareOnline())
                return true;
        }
        else {
            transUI.showError(timeout, T_void_card_not_same);
            return false;
        }
        return false;
    }

    private boolean PBOCTrans() {
        int code = 0;

        PBOCTransProperty property = new PBOCTransProperty();
        property.setTag9c(PBOCTag9c.sale);
        property.setTraceNO(Integer.parseInt(TraceNo));
        property.setFirstEC(false);
        property.setForceOnline(true);
        property.setAmounts(Amount);
        property.setOtherAmounts(0);
        property.setIcCard(false);

        transUI.handling(timeout, Tcode.Status.process_trans);

        emvl2 = new EmvL2Process(this.context, para);
        emvl2.setTraceNo(TraceNo);//JM
        emvl2.setTypeTrans(TransEName);
        if ((retVal = emvl2.emvl2ParamInit()) != 0) {
            switch (retVal) {
                case 1:
                    retVal = Tcode.T_err_not_file_terminal;
                    break;
                case 2:
                    retVal = Tcode.T_err_not_file_processing;
                    break;
                case 3:
                    retVal = Tcode.T_err_not_file_entry_point;
                    break;
            }
            transUI.showError(timeout, retVal);
            return false;
        }
        emvl2.SetAmount(Amount, 0);
        emvl2.setTypeCoin(typeCoin);//JM
        code = emvl2.start();

        Logger.debug("EmvL2Process return = " + code);
        if (code != 0) {
            if (code==7){
                retVal=Tcode.T_insert_card;
            }else{
                retVal=Tcode.T_err_detect_card_failed;
            }
            transUI.showError(timeout, retVal);
            return false;
        }

        Pan = emvl2.GetCardNo();
        PanSeqNo = emvl2.GetPanSeqNo();
        Track2 = emvl2.GetTrack2data();
        ICCData = emvl2.GetEmvOnlineData();
        Logger.error("PAN =" + Pan);

        if (!incardTable(Pan, TransEName)) {
            transUI.showError(timeout, Tcode.T_unsupport_card);
            return false;
        }

        if (data.getPanNormal().equals(Pan)) {
            if(handlePBOCode(PBOCode.PBOC_REQUEST_ONLINE))
                return true;
        } else {
            transUI.showError(timeout, T_void_card_not_same);
            return false;
        }
        return false;
    }

    /**
     * handle PBOC transaction
     *
     * @param code Code
     */
    private boolean handlePBOCode(int code) {
        if (code != PBOCode.PBOC_REQUEST_ONLINE) {
            transUI.showError(timeout, code);
            return false;
        }
        if (inputMode != ENTRY_MODE_NFC)
            setICCDataCTL();

        if (prepareOnline())
            return true;

        return false;
    }

    private boolean isHandlePagoPrevoucher() {
        if ((retVal = CommonFunctionalities.setPanManual(timeout, TransEName, transUI)) != 0) {
            return false;
        }

        Pan = CommonFunctionalities.getPan();

        if (!incardTable(Pan, TransEName)) {
            transUI.showError(timeout, Tcode.T_unsupport_card);
            return false;
        }

        if (data.getPanNormal().equals(Pan)) {
            if (prepareOnline())
                return true;
        }
        else {
            transUI.showError(timeout, T_void_card_not_same);
            return false;
        }
        return false;
    }

    private boolean setPropinaPagoPrevoucher(){
        long pulTemp = 0;
        boolean ret = false;

        while (true) {
            InputInfo inputInfo = transUI.getOutsideInput(timeout, InputManager.Mode.AMOUNT, "INGRESE PROPINA");
            if (inputInfo.isResultFlag()) {
                TipAmount = Long.parseLong(inputInfo.getResult());

                long propinaPercent = Long.parseLong(tconf.getPORCENTAJE_MAXIMO_PROPINA());

                if (propinaPercent <= 0) {
                    TipAmount = 0;
                    retVal = Tcode.T_user_cancel_operation;
                    transUI.showError(timeout, Tcode.T_user_cancel_operation);
                    ret = false;
                }

                pulTemp = data.getAmmountXX() + data.getAmmount0();

                pulTemp = calcularPorcentaje(pulTemp, propinaPercent);

                if (TipAmount > pulTemp) {
                    inputInfo = transUI.showMessageInfo(data.getTransEName(), "PROPINA MAYOR A LA" + "\n" + "PERMITIDA", "CANCELAR", "REINTENTAR", timeout);
                    if (!inputInfo.isResultFlag()) {
                        retVal = Tcode.T_user_cancel_operation;
                        transUI.showError(timeout, Tcode.T_user_cancel_operation);
                        ret = false;
                    }
                } else {
                    ret = true;
                    break;
                }
            } else {
                retVal = Tcode.T_user_cancel_operation;
                transUI.showError(timeout, Tcode.T_user_cancel_operation);
                ret = false;
            }
        }
        return ret;
    }

    private long calcularPorcentaje(long monto, long porcentaje) {
        double result = (double) porcentaje / 100;
        double iva = monto * result;
        double ivaFinal = Math.round(iva);
        return (long) ivaFinal;
    }

    private boolean prepareOnline() {

        transLog = TransLog.getInstance(idAcquirer);

        if ((retVal = CommonFunctionalities.setTipoCuenta(timeout, ProcCode, transUI, ISOUtil.stringToBoolean(rango.getTIPO_DE_CUENTA()))) != 0) {
            return false;
        }

        ProcCode = CommonFunctionalities.getProCode();

        if ((retVal = CommonFunctionalities.setPrompt(timeout, TransEName, listPrompts, transUI)) != 0) {
            return false;
        }

        Field58 = CommonFunctionalities.getFld58Prompts();

        if (retVal == 0) {

            transUI.handling(timeout, Tcode.Status.connecting_center);
            setDatas(inputMode);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                retVal = OnlineTrans(emv);
            } else {
                retVal = OnlineTrans(null);
            }
            Logger.debug("Prevoucher>>OnlineTrans=" + retVal);
            clearPan();
            if (retVal == 0) {
                data.setVoided(true);
                int index = TransLog.getInstance(idLote + FILE_NAME_PREVOUCHER).getCurrentIndex(data);
                TransLog.getInstance(idLote + FILE_NAME_PREVOUCHER).deleteTransLog(index);
                TransLog.getInstance(idLote + FILE_NAME_PREVOUCHER).saveLog(data, idLote + FILE_NAME_PREVOUCHER);

                msgAprob(Tcode.Status.pago_prevoucher_exitoso,true);
                return true;

            }else {
                transUI.showError(timeout, retVal);
                return false;
            }

        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
    }

}
