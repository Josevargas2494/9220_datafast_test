package com.datafast.transactions.pre_voucher;

import android.content.Context;
import android.media.ToneGenerator;

import com.android.newpos.libemv.PBOCTag9c;
import com.android.newpos.libemv.PBOCTransProperty;
import com.android.newpos.libemv.PBOCode;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.transactions.common.GetAmount;
import com.newpos.bypay.EmvL2CVM;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.contactless.EmvL2Process;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.util.Objects;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.master.MasterControl;

import static cn.desert.newpos.payui.master.MasterControl.incardTable;
import static com.android.newpos.pay.StartAppDATAFAST.listPrompts;
import static com.android.newpos.pay.StartAppDATAFAST.rango;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_FALLBACK;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_SWIPE_ICC_CTL;
import static com.datafast.menus.menus.FALLBACK;
import static com.datafast.menus.menus.TOTAL_BATCH;
import static com.datafast.menus.menus.contFallback;
import static com.datafast.menus.menus.idAcquirer;
import static com.datafast.transactions.common.GetAmount.checkTip;
import static com.datafast.transactions.common.GetAmount.getConfirmarMontos;

public class PreVoucher extends FinanceTrans implements TransPresenter {

    /**
     * @param ctx        Context
     * @param transEname TransEname
     */

    public PreVoucher(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);
        init(transEname, p);
    }

    private void init(String transEname, TransInputPara p) {
        para = p;
        transUI = para.getTransUI();
        isReversal = false;
        isProcPreTrans = true;
        isSaveLog = true;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
    }

    @Override
    public ISO8583 getISO8583() {
        return null;
    }

    @Override
    public void start() {

        if(!checkBatchAndSettle(true,true))
            return;

        if (!setAmount())
            return;

        if (!CardProcess(INMODE_IC | INMODE_MAG | INMODE_NFC | INMODE_HAND))
            return;

        if (!prepareOffline())
            return;

        Logger.debug("PreVoucherTrans>>finish");
        return;
    }

    /**
     * 准备联机
     */
    private boolean prepareOffline() {

        if ((retVal = CommonFunctionalities.setTipoCuenta(timeout, ProcCode, transUI, ISOUtil.stringToBoolean(rango.getTIPO_DE_CUENTA()))) != 0) {
            return false;
        }

        ProcCode = CommonFunctionalities.getProCode();

        if ((retVal = CommonFunctionalities.confirmAmount(timeout, TransEName, transUI, getConfirmarMontos())) != 0) {
            return false;
        }

        if (!requestPin()) {
            return false;
        }

        if (retVal == 0) {
            if ((retVal = CommonFunctionalities.setPrompt(timeout, TransEName, listPrompts, transUI)) != 0) {
                return false;
            }

            Field58 = CommonFunctionalities.getFld58Prompts();

            setDatas(inputMode);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                retVal = OfflineTrans();
            } else {
                retVal = OfflineTrans();
            }
            Logger.debug("Prevoucher>>OffineTrans=" + retVal);
            clearPan();
            if (retVal == 0) {
                msgAprob(Tcode.Status.prevoucher_exitoso, false);
                return true;
            }else
                return false;
        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
    }
}
