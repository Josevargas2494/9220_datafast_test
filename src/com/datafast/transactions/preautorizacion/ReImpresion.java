package com.datafast.transactions.preautorizacion;

import android.content.Context;
import android.media.ToneGenerator;

import com.android.newpos.libemv.PBOCTag9c;
import com.android.newpos.libemv.PBOCTransProperty;
import com.android.newpos.libemv.PBOCode;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.transactions.common.GetAmount;
import com.newpos.bypay.EmvL2CVM;
import com.newpos.libpay.Logger;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.contactless.EmvL2Process;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.presenter.TransPresenter;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.TransInputPara;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.util.Objects;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.master.MasterControl;

import static cn.desert.newpos.payui.master.MasterControl.incardTable;
import static com.android.newpos.pay.StartAppDATAFAST.rango;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_FALLBACK;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.GERCARD_MSG_SWIPE_ICC_CTL;
import static com.datafast.menus.menus.FALLBACK;
import static com.datafast.menus.menus.contFallback;
import static com.datafast.menus.menus.idAcquirer;

public class ReImpresion extends FinanceTrans implements TransPresenter {

    public ReImpresion(Context ctx, String transEname, TransInputPara p) {
        super(ctx, transEname);
        //super(ctx, transEname, FILE_NAME_PREAUTO);

        para = p;
        transUI = para.getTransUI();
        isReversal = true;
        isProcPreTrans = true;
        isSaveLog = false;
        isDebit = true;
        TransEName = transEname;
        currency_name = CommonFunctionalities.tipoMoneda()[0];
        typeCoin = CommonFunctionalities.tipoMoneda()[1];
        host_id = idAcquirer;
    }

    @Override
    public ISO8583 getISO8583() {
        return null;
    }

    @Override
    public void start() {

        if(!checkBatchAndSettle(false,true))
            return;

        if (!procesarReimpresion())
            return;

        Logger.debug("voidPreAutoTrans>>finish");
        return;
    }

    private boolean procesarReimpresion() {

        if ((retVal = CommonFunctionalities.setNumReferencia(timeout, "REIMPRESION", transUI)) != 0) {
            return false;
        }

        if ((retVal = CommonFunctionalities.setIdPreAutoAmpliacion(timeout, "REIMPRESION", transUI)) != 0) {
            return false;
        }

        IdPreAutAmpl = CommonFunctionalities.getIdPreAutoAmpliacion();
        Pan = CommonFunctionalities.getPan();
        TraceNo = ISOUtil.padleft("" + CommonFunctionalities.getNumReferencia(), 6, '0');
        AuthCode = "000000";

        if(setAmount()) {
            if (CardProcess(INMODE_IC | INMODE_MAG | INMODE_NFC | INMODE_HAND))
                if (prepareOnline())
                    return true;
        }

        return false;
    }

    /**
     * 准备联机
     */
    private boolean prepareOnline() {

        if ((retVal = CommonFunctionalities.confirmarDatos(timeout, transUI, Pan, IdPreAutAmpl, TransEName)) != 0) {
            return false;
        }

        if (!requestPin()) {
            return false;
        }

        if (retVal == 0) {

            transUI.handling(timeout, Tcode.Status.connecting_center);
            setDatas(inputMode);
            if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
                retVal = OnlineTrans(emv);
            } else {
                retVal = OnlineTrans(null);
            }
            Logger.debug("Reimpresion>>OnlineTrans=" + retVal);
            clearPan();
            if (retVal == 0) {
                msgAprob(Tcode.Status.reprint_exitosa,true);
                return true;
            }else {
                transUI.showError(timeout, retVal);
                return false;
            }

        } else {
            transUI.showError(timeout, retVal);
            return false;
        }
    }
}
