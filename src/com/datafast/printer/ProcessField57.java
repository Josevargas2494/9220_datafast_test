package com.datafast.printer;

public class ProcessField57 {

    public static final int NOMBRE_COMERCIO = 0;
    public static final int MID = 1;
    public static final int VALOR_FINANCIACION = 2;
    public static final int VALOR_TRANS = 3;
    public static final int VOUCHER_RAPIDO_FAST_CLUB = 4;
    public static final int PUBLICIDAD_MIN = 5;
    public static final int PUBLICIDAD_MAY = 6;
    public static final int PIN = 7;
    public static final int VIGENCIA = 8;
    public static final int VOUCHER_RAPIDO_PACIFICARD = 9;
    public static final int SECURY_CODE = 10;
    public static final int NUM_TEL_RECARGADOR = 11;
    public static final int NUM_TRAN_PROVE = 12;
    public static final int REDENCION = 13;
    public static final int NOMBRE_DUENO_CUENTA = 14;
    public static final int VALOR_PAGAR_CONS = 15;
    public static final int COD_ERROR = 16;

    public static final int ID_001 = 0;
    public static final int ID_002 = 1;
    public static final int ID_003 = 2;
    public static final int ID_004 = 3;
    public static final int ID_005 = 4;
    public static final int ID_007 = 5;
    public static final int ID_008 = 6;
    public static final int ID_009 = 7;
    public static final int ID_011 = 8;
    public static final int ID_012 = 9;
    public static final int ID_013 = 10;
    public static final int ID_014 = 11;
    public static final int ID_015 = 12;
    public static final int ID_016 = 13;
    public static final int ID_017 = 14;
    public static final int ID_018 = 15;
    public static final int ID_019 = 16;
    public static final int ID_020 = 17;
    public static final int ID_021 = 18;
    public static final int ID_022 = 19;
    public static final int ID_023 = 20;
    public static final int ID_025 = 21;

    private String[] rspField57;
    private String[] identificadoresActivos;

    public ProcessField57() {
        rspField57 = new String[17];
        identificadoresActivos = new String[25];
    }

    public String[] getRspField57() {
        return rspField57;
    }

    public String[] getIdentificadoresActivos() {
        return identificadoresActivos;
    }

    public void publicidad(String msg) {
        int pos = msg.indexOf("008");

        if (pos != -1) {
            identificadoresActivos[ID_008] = "008";
            rspField57[PUBLICIDAD_MAY] = msg.substring(pos + 3);
        }

        pos = msg.indexOf("009");

        if (pos != -1) {
            identificadoresActivos[ID_009] = "009";
            rspField57[PUBLICIDAD_MIN] = msg.substring(pos + 3);
        }
    }

    public void verificarVoucherRapido(String msg) {

        if (msg.length()>= 3){
            String tag = msg.substring(msg.length()-3);
            if (tag.equals("016")){
                identificadoresActivos[ID_016] = "016";
                rspField57[VOUCHER_RAPIDO_FAST_CLUB] = tag;
            }
            if (tag.equals("025")){
                identificadoresActivos[ID_025] = "025";
                rspField57[VOUCHER_RAPIDO_PACIFICARD] = tag;
            }
        }
    }

    public void valorFinanciacion(String msg) {
        int pos = msg.indexOf("004");
        if (pos != -1) {
            identificadoresActivos[ID_004] = "004";
        }
    }

    public void montoFijo(String msg) {
        int pos = msg.indexOf("014");
        if (pos != -1) {
            identificadoresActivos[ID_014] = "014";
        }
    }

    public void clearP57() {
        for (int i = 0; i < rspField57.length; i++) {
            rspField57[i] = "-";
        }
    }

    public void clearIdentificadores() {
        for (int i = 0; i < identificadoresActivos.length; i++) {
            identificadoresActivos[i] = "-";
        }
    }

    public String printField57(String fld57) {
        String id = "";

        clearP57();
        clearIdentificadores();

        try {
            if (fld57 != null) {
                id = fld57.substring(0, 3);
                String msg = fld57.substring(3);

                switch (id) {
                    case "001":                                                             //Security Code
                        identificadoresActivos[ID_001] = id;
                        rspField57[SECURY_CODE] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "002":                                                             //Numero de Telefono recargador
                        identificadoresActivos[ID_002] = id;
                        rspField57[NUM_TEL_RECARGADOR] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "003":                                                             //Numero de transaccion en el proveedor
                        identificadoresActivos[ID_003] = id;
                        rspField57[NUM_TRAN_PROVE] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "004":                                                             //Valor de la financiación
                        identificadoresActivos[ID_004] = id;
                        rspField57[VALOR_FINANCIACION] = msg.substring(0, 8);                //Valor de la financiación
                        rspField57[VALOR_TRANS] = msg.substring(8, 16);                          //Total de transaccion
                        verificarVoucherRapido(msg.substring(16));
                        break;
                    case "005":                                                             //Redencion
                        identificadoresActivos[ID_005] = id;
                        rspField57[REDENCION] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "007":                                                            //Nombre y MID del POS
                        identificadoresActivos[ID_007] = id;
                        rspField57[NOMBRE_COMERCIO] = msg.substring(0, 16);                  //Nombre
                        rspField57[MID] = msg.substring(16, 26);                              //MID
                        verificarVoucherRapido(msg);
                        break;
                    case "008":                                                            //Mensaje de publicidad en Mayusculas
                        msg = msg.toUpperCase();
                        identificadoresActivos[ID_008] = id;
                        rspField57[PUBLICIDAD_MAY] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "009":                                                            //Mensaje de publicidad en Minusculas
                        msg = msg.toLowerCase();
                        identificadoresActivos[ID_009] = id;
                        rspField57[PUBLICIDAD_MIN] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "011":                                                            //Nombre del dueño de la cuenta
                        identificadoresActivos[ID_011] = id;
                        rspField57[NOMBRE_DUENO_CUENTA] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "012":                                                            //Codigo de error
                        identificadoresActivos[ID_012] = id;
                        rspField57[COD_ERROR] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "013":                                                            //Valor a pagar en consultas
                        identificadoresActivos[ID_013] = id;
                        rspField57[VALOR_PAGAR_CONS] = msg;
                        verificarVoucherRapido(msg);
                        break;
                    case "014":                                                            //Valor del monto fijo
                        identificadoresActivos[ID_014] = id;
                        rspField57[VALOR_FINANCIACION] = msg.substring(0, 8);                //Valor de la financiación
                        rspField57[VALOR_TRANS] = msg.substring(8, 16);                          //Total de transaccion
                        verificarVoucherRapido(msg.substring(16));
                        break;
                    case "015":                                                            //PIN y dias de vigencia
                        identificadoresActivos[ID_015] = id;
                        rspField57[PIN] = msg.substring(0, 15);                              //PIN
                        rspField57[VIGENCIA] = msg.substring(15);                            //Vigencia
                        verificarVoucherRapido(msg);
                        break;
                    case "016":                                                            //Codigo voucher rapido
                        identificadoresActivos[ID_016] = id;
                        rspField57[VOUCHER_RAPIDO_FAST_CLUB] = msg;
                        break;
                    case "017":                                                            //Nombre + MID + Valor financiacion
                        identificadoresActivos[ID_017] = id;
                        rspField57[NOMBRE_COMERCIO] = msg.substring(0, 16);                  //Nombre
                        rspField57[MID] = msg.substring(16, 26);                            //MID
                        valorFinanciacion(msg.substring(26, 29));
                        rspField57[VALOR_FINANCIACION] = msg.substring(29, 37);             //Valor financiacion
                        rspField57[VALOR_TRANS] = msg.substring(37, 45);                    //Total transaccion
                        verificarVoucherRapido(msg.substring(45));                                       //Codigo voucher rapido
                        break;
                    case "018":
                        identificadoresActivos[ID_018] = id;
                        rspField57[NOMBRE_COMERCIO] = msg.substring(0, 16);
                        rspField57[MID] = msg.substring(16, 26);
                        valorFinanciacion(msg.substring(26, 29));
                        rspField57[VALOR_FINANCIACION] = msg.substring(29, 37);
                        rspField57[VALOR_TRANS] = msg.substring(37, 45);
                        publicidad(msg.substring(45));
                        verificarVoucherRapido(msg);
                        break;
                    case "021":                                                             //Datos de interés + mensaje de publicidad
                        identificadoresActivos[ID_021] = id;
                        valorFinanciacion(msg.substring(0, 3));
                        rspField57[VALOR_FINANCIACION] = msg.substring(3, 11);
                        rspField57[VALOR_TRANS] = msg.substring(11, 19);
                        publicidad(msg.substring(19));
                        verificarVoucherRapido(msg);
                        break;
                    case "023":
                        identificadoresActivos[ID_023] = id;
                        montoFijo(msg.substring(0, 3));
                        rspField57[VALOR_FINANCIACION] = msg.substring(3, 11);                //Valor de la financiación
                        rspField57[VALOR_TRANS] = msg.substring(11, 19);                          //Total de transaccion
                        publicidad(msg.substring(19));
                        verificarVoucherRapido(msg);
                        break;
                    case "025":                                                            //Pagos Electronicos
                        identificadoresActivos[ID_025] = id;
                        rspField57[VOUCHER_RAPIDO_PACIFICARD] = msg;
                        break;
                }
            }
            else
                return null;

        }catch (IndexOutOfBoundsException e){
            return null;
        }

        return id;
    }
}
