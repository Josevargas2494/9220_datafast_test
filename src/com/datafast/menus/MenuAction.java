package com.datafast.menus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.newpos.pay.R;
import com.datafast.definesDATAFAST.DefinesDATAFAST;
import com.datafast.inicializacion.trans_init.Init;
import com.datafast.printer.PrintParameter;
import com.datafast.tools.WifiSettings;
import com.datafast.tools_bacth.ToolsBatch;
import com.datafast.transactions.Settle.Settle;
import com.datafast.transactions.callbacks.makeInitCallback;
import com.datafast.transactions.callbacks.waitPrintReport;
import com.datafast.transactions.callbacks.waitSeatleReport;
import com.datafast.transactions.common.CommonFunctionalities;
import com.datafast.updateapp.UpdateApk;
import com.newpos.libpay.device.printer.PrintRes;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.pos.device.printer.Printer;

import java.io.File;
import java.util.List;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.master.MasterControl;
import cn.desert.newpos.payui.setting.ui.simple.CommunSettings;
import cn.desert.newpos.payui.transrecord.HistoryTrans;

import static android.content.Context.WIFI_SERVICE;
import static com.android.newpos.pay.StartAppDATAFAST.batteryStatus;
import static com.android.newpos.pay.StartAppDATAFAST.isInit;
import static com.android.newpos.pay.StartAppDATAFAST.paperStatus;
import static com.android.newpos.pay.StartAppDATAFAST.readWriteFileMDM;
import static com.android.newpos.pay.StartAppDATAFAST.tconf;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.FILE_NAME_PREAUTO;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.FILE_NAME_PREVOUCHER;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.ITEM_PAGO_PREVOUCHER;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.ITEM_PREAUTO_PANTALLA;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.ITEM_PRE_VOUCHER;
import static com.datafast.definesDATAFAST.DefinesDATAFAST.ITEM_REPORTE_DETALLADO;
import static com.datafast.menus.menus.idAcquirer;
import static com.newpos.libpay.trans.Trans.idLote;
import static com.android.newpos.pay.StartAppDATAFAST.makeinitcallback;

//import static com.datafast.menus.menus.acquirerRow;
//import static com.datafast.menus.menus.issuerRow;

public class MenuAction {

    private Context context;
    private String tipoDeMenu;
    private String Estado;

    private Dialog mDialog;

    //Claves para cuando no esta inicializado el POS
    private final String TERMINAL_PWD = "000000";
    private final String COMERCIO_PWD = "0000";

    public static String JUMP_KEY = "JUMP_KEY";

    public static waitPrintReport callbackPrint;
    public static waitSeatleReport callBackSeatle;
    //public static makeInitCallback makeInitCallback;

    MenuAction(Context context, String tipoDeMenu) {
        this.context = context;
        this.tipoDeMenu = tipoDeMenu;
        MasterControl.setMcontext(context);
    }

    void SelectAction() {
        Intent intent = new Intent();

        switch (tipoDeMenu) {
            case DefinesDATAFAST.ITEM_VENTA:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[21]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_DIFERIDO:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[26]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_PAGOS_ELECTRONICOS:
                if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_PAGOS_ELECTRONICOS())){
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setClass(context, MasterControl.class);
                    intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[27]);
                    context.startActivity(intent);
                }else{
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, "LOS PAGOS ELECTRONICOS \n NO ESTAN HABILITADOS", Toast.LENGTH_SHORT);
                }
                break;
            case DefinesDATAFAST.ITEM_TRANS_PRE_AUT:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[28]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_AMPLIACION:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[29]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_CONFIRMACION:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[30]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_ANULACION_PRE_AUT:
                if (isInit)
                    maintainPwd("CLAVE DE COMERCIO", tconf.getCLAVE_COMERCIO(), DefinesDATAFAST.ITEM_ANULACION_PRE_AUT, 4);
                else
                    maintainPwd("CLAVE DE COMERCIO", COMERCIO_PWD, DefinesDATAFAST.ITEM_ANULACION_PRE_AUT, 4);

                break;
            case DefinesDATAFAST.ITEM_REIMPRESION_PRE_AUT:
                if (isInit)
                    maintainPwd("CLAVE DE COMERCIO", tconf.getCLAVE_COMERCIO(), DefinesDATAFAST.ITEM_REIMPRESION_PRE_AUT, 4);
                else
                    maintainPwd("CLAVE DE COMERCIO", COMERCIO_PWD, DefinesDATAFAST.ITEM_REIMPRESION_PRE_AUT, 4);
                break;
            case DefinesDATAFAST.ITEM_ECHO_TEST:
                if (isInit) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setClass(context, MasterControl.class);
                    intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[20]);
                    context.startActivity(intent);
                }else{
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, "Debe Inicializar POS!", Toast.LENGTH_LONG);
                }

                break;
            case DefinesDATAFAST.ITEM_ANULACION:
                if (isInit)
                    maintainPwd("CLAVE DE COMERCIO", tconf.getCLAVE_COMERCIO(), DefinesDATAFAST.ITEM_ANULACION, 4);
                else
                    maintainPwd("CLAVE DE COMERCIO", COMERCIO_PWD, DefinesDATAFAST.ITEM_ANULACION, 4);

                break;
            case DefinesDATAFAST.ITEM_DEPOSITO:
                if (isInit)
                    maintainPwd("CLAVE DE COMERCIO", tconf.getCLAVE_COMERCIO(), DefinesDATAFAST.ITEM_DEPOSITO, 4);
                else
                    maintainPwd("CLAVE DE COMERCIO", COMERCIO_PWD, DefinesDATAFAST.ITEM_DEPOSITO, 4);

                break;
            case DefinesDATAFAST.ITEM_CONFIGURACION:
                if (isInit)
                    maintainPwd("CLAVE TECNICO", tconf.getCLAVE_TECNICO(), DefinesDATAFAST.ITEM_CONFIGURACION, 6);
                else
                    maintainPwd("CLAVE TECNICO", TERMINAL_PWD, DefinesDATAFAST.ITEM_CONFIGURACION, 6);

                break;
            case DefinesDATAFAST.ITEM_TEST:
                if ((batteryStatus.getLevelBattery() <= 8) && (!batteryStatus.isCharging())) {
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_BATTERY, Toast.LENGTH_SHORT);
                } else if (paperStatus.getRet() == Printer.PRINTER_STATUS_PAPER_LACK){
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_PAPER, Toast.LENGTH_SHORT);
                } else {
                    intent = new Intent(context, PrintParameter.class);
                    intent.putExtra("typeReport", DefinesDATAFAST.ITEM_TEST);
                    context.startActivity(intent);
                }
                break;
            case DefinesDATAFAST.ITEM_TRANS_EN_PANTALLA:
                if ((batteryStatus.getLevelBattery() <= 8) && (!batteryStatus.isCharging())) {
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_BATTERY, Toast.LENGTH_SHORT);
                } else if (paperStatus.getRet() == Printer.PRINTER_STATUS_PAPER_LACK){
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_PAPER, Toast.LENGTH_SHORT);
                } else {

                    idAcquirer = idLote;
                    if (ToolsBatch.statusTrans(idAcquirer)) {
                        intent = new Intent(context, HistoryTrans.class);
                        intent.putExtra(HistoryTrans.EVENTS, HistoryTrans.COMMON);
                        context.startActivity(intent);
                    } else {
                        UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.LOTE_VACIO, Toast.LENGTH_LONG);
                        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                        toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                    }
                }
                break;
            case ITEM_PREAUTO_PANTALLA:
                if ((batteryStatus.getLevelBattery() <= 8) && (!batteryStatus.isCharging())) {
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_BATTERY, Toast.LENGTH_SHORT);
                } else if (paperStatus.getRet() == Printer.PRINTER_STATUS_PAPER_LACK){
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_PAPER, Toast.LENGTH_SHORT);
                } else {

                    idAcquirer = idLote + FILE_NAME_PREAUTO;
                    if (ToolsBatch.statusTrans(idAcquirer)) {
                        intent = new Intent(context, HistoryTrans.class);
                        intent.putExtra(HistoryTrans.EVENTS, HistoryTrans.COMMON);
                        context.startActivity(intent);
                    } else {
                        UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.LOTE_VACIO_PREAUTO, Toast.LENGTH_LONG);
                        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                        toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                    }
                }
                break;
            case DefinesDATAFAST.ITEM_BORRAR_LOTE:
                if (isInit)
                    maintainPwd("CLAVE TECNICO", tconf.getCLAVE_TECNICO(), DefinesDATAFAST.ITEM_BORRAR_LOTE, 6);
                else
                    maintainPwd("CLAVE TECNICO", TERMINAL_PWD, DefinesDATAFAST.ITEM_BORRAR_LOTE, 6);

                break;
            case DefinesDATAFAST.ITEM_BORRAR_REVERSO:
                if (isInit)
                    maintainPwd("CLAVE TECNICO", tconf.getCLAVE_TECNICO(), DefinesDATAFAST.ITEM_BORRAR_REVERSO, 6);
                else
                    maintainPwd("CLAVE TECNICO", TERMINAL_PWD, DefinesDATAFAST.ITEM_BORRAR_REVERSO, 6);
                break;
            case DefinesDATAFAST.ITEM_INICIALIZACION:
                idAcquirer = idLote;
                if (!ToolsBatch.statusTrans(idAcquirer) && !ToolsBatch.statusTrans(idAcquirer + FILE_NAME_PREAUTO)) {
                    //if (!ToolsBatch.statusTrans(context)) {
                    menus.pause = true;
                    menus.deleteTimerMenus();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setClass(context, Init.class);
                    intent.putExtra("PARCIAL", false);
                    context.startActivity(intent);
                } else {
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_SETTLE, Toast.LENGTH_SHORT);
                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                    toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                }
                break;
            case DefinesDATAFAST.ITEM_CONFIG_INICIAL:
                menus.pause = true;
                menus.deleteTimerMenus();
                String text = "CONFIG INICIAL";
                intent.putExtra(JUMP_KEY, text);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, CommunSettings.class);
                context.startActivity(intent);
                break;
            case ITEM_REPORTE_DETALLADO:
                if ((batteryStatus.getLevelBattery() <= 8) && (!batteryStatus.isCharging())) {
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_BATTERY, Toast.LENGTH_SHORT);
                } else if (paperStatus.getRet() == Printer.PRINTER_STATUS_PAPER_LACK){
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_PAPER, Toast.LENGTH_SHORT);
                } else {
                    callbackPrint = null;
                    idAcquirer = idLote;
                    if (ToolsBatch.statusTrans(idAcquirer) || ToolsBatch.statusTrans(idAcquirer + FILE_NAME_PREAUTO)) {
                        PrintParameter.setPrintTotals(true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setClass(context, PrintParameter.class);
                        intent.putExtra("typeReport", DefinesDATAFAST.ITEM_REPORTE_DETALLADO);
                        context.startActivity(intent);
                    } else {
                        UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.LOTE_VACIO, Toast.LENGTH_SHORT);
                        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                        toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                    }
                }
                break;
            case ITEM_PRE_VOUCHER:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[33]);
                context.startActivity(intent);
                break;
            case ITEM_PAGO_PREVOUCHER:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[34]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_CASH_OVER:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[35]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_PAGOS_VARIOS:
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(context, MasterControl.class);
                intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[36]);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_CONFIG_WIFI:
                menus.pause = true;
                menus.deleteTimerMenus();
                intent.setClass(context, WifiSettings.class);
                context.startActivity(intent);
                break;
            case DefinesDATAFAST.ITEM_APPMANAGER:
                idAcquirer = idLote;
                if (!ToolsBatch.statusTrans(idAcquirer) && !ToolsBatch.statusTrans(idAcquirer + FILE_NAME_PREAUTO)) {
                    menus.pause = true;
                    menus.deleteTimerMenus();
                    Intent intentManager = context.getPackageManager().getLaunchIntentForPackage("com.newpos.appmanager");
                    context.startActivity(intentManager);
                } else {
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_SETTLE, Toast.LENGTH_SHORT);
                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                    toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                }
                break;
            default:
                intent.setClass(context, menus.class);
                intent.putExtra(DefinesDATAFAST.DATO_MENU, tipoDeMenu);
                context.startActivity(intent);
                break;
        }
    }

    private void maintainPwd(String title, final String pwd, final String type_trans, int lenEdit) {
        final Intent intent = new Intent();
        mDialog = UIUtils.centerDialog(context, R.layout.setting_home_pass, R.id.setting_pass_layout);
        final EditText newEdit = mDialog.findViewById(R.id.setting_pass_new);
        final TextView title_pass = mDialog.findViewById(R.id.title_pass);
        Button confirm = mDialog.findViewById(R.id.setting_pass_confirm);
        newEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(lenEdit)});
        newEdit.requestFocus();
        title_pass.setText(title);

        mDialog.findViewById(R.id.setting_pass_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String np = newEdit.getText().toString();

                if (np.equals(pwd)) {

                    MasterControl.hideKeyBoard(newEdit.getWindowToken(), context);

                    switch (type_trans) {
                        case DefinesDATAFAST.ITEM_DEPOSITO:

                            TransLogData revesalData = TransLog.getReversal();
                            if (revesalData != null) {
                                UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_REV_PEN, Toast.LENGTH_SHORT);
                            }else {

                                if ((batteryStatus.getLevelBattery() <= 8) && (!batteryStatus.isCharging())) {
                                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_BATTERY, Toast.LENGTH_SHORT);
                                } else if (paperStatus.getRet() == Printer.PRINTER_STATUS_PAPER_LACK) {
                                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_PAPER, Toast.LENGTH_SHORT);
                                } else {
                                    idAcquirer = idLote;
                                    if (ToolsBatch.statusTrans(idAcquirer) || ToolsBatch.statusTrans(idAcquirer + FILE_NAME_PREAUTO)) {
                                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                                        alertDialog.setIcon(R.drawable.ic_launcher);
                                        alertDialog.setTitle("INFORMACIÓN");
                                        alertDialog.setMessage("¿IMPRIMIR RESUMEN DE VENTAS?");
                                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Si",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(final DialogInterface dialog, int which) {

                                                        PrintParameter.setPrintTotals(true);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.setClass(context, PrintParameter.class);
                                                        intent.putExtra("typeReport", DefinesDATAFAST.ITEM_REPORTE_DETALLADO);
                                                        context.startActivity(intent);

                                                        callbackPrint = null;
                                                        callbackPrint = new waitPrintReport() {
                                                            @Override
                                                            public void getRspPrintReport(int status) {

                                                                if (status == 0) {
                                                                    PrintParameter.setPrintTotals(false);
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.setClass(context, MasterControl.class);
                                                                    intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[5]);
                                                                    context.startActivity(intent);
                                                                }

                                                                dialog.dismiss();
                                                            }
                                                        };
                                                    }
                                                });
                                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        PrintParameter.setPrintTotals(false);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.setClass(context, MasterControl.class);
                                                        intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[5]);
                                                        context.startActivity(intent);
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();

                                        makeinitcallback = null;

                                        makeinitcallback = new makeInitCallback() {
                                            @Override
                                            public void getMakeInitCallback(boolean status) {
                                                //if (Settle.isSeatleOk) {
                                                if (status) {
                                                    try{

                                                        UpdateApk updateApk = new UpdateApk(context);
                                                        updateApk.instalarApp(context);

                                                        if (readWriteFileMDM.getInitAuto().equals(readWriteFileMDM.INITAUTODEACTIVE)){
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setClass(context, Init.class);
                                                            intent.putExtra("PARCIAL", true);
                                                            context.startActivity(intent);
                                                        }
                                                    }catch (Exception e){
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        };

                                    } else {
                                        UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.LOTE_VACIO, Toast.LENGTH_SHORT);
                                        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                                        toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                                    }
                                }
                            }
                            break;
                        case DefinesDATAFAST.ITEM_BORRAR_REVERSO:
                            if (!TransLog.clearReveral()) {
                                UIUtils.toast((Activity) context, R.drawable.ic_launcher, "REVERSO BORRADO EXITOSAMENTE", Toast.LENGTH_SHORT);
                                ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                                toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                            } else
                                UIUtils.toast((Activity) context, R.drawable.ic_launcher, "NO EXISTE REVERSO", Toast.LENGTH_SHORT);

                            break;
                        case DefinesDATAFAST.ITEM_BORRAR_LOTE:

                            TransLogData revesalData1 = TransLog.getReversal();
                            if (revesalData1 != null) {
                                UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_REV_PEN, Toast.LENGTH_SHORT);
                            }else {
                                if (paperStatus.getRet() == Printer.PRINTER_STATUS_PAPER_LACK) {
                                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.MSG_PAPER, Toast.LENGTH_SHORT);
                                } else {

                                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                                    alertDialog.setIcon(R.drawable.ic_launcher);
                                    alertDialog.setTitle("INFORMACIÓN");
                                    alertDialog.setMessage("¿DESEA BORRAR LOTE?");
                                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Si",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(final DialogInterface dialog, int which) {

                                                    callbackPrint = null;
                                                    callbackPrint = new waitPrintReport() {
                                                        @Override
                                                        public void getRspPrintReport(int status) {

                                                            if (status == 0) {
                                                                idAcquirer = idLote + FILE_NAME_PREAUTO;
                                                                if (ToolsBatch.statusTrans(idAcquirer)) {
                                                                    if (idAcquirer != null) {
                                                                        if (ToolsBatch.statusTrans(idAcquirer)) {
                                                                            int val = Integer.parseInt(TMConfig.getInstance().getBatchNo());
                                                                            TMConfig.getInstance().setBatchNo(val).save();
                                                                            TransLog.getInstance(idAcquirer).clearAll(idAcquirer);
                                                                            CommonFunctionalities.limpiarPanTarjGasolinera("");
                                                                        }
                                                                    }
                                                                }

                                                                idAcquirer = idLote;
                                                                if (ToolsBatch.statusTrans(idAcquirer)) {
                                                                    int val = Integer.parseInt(TMConfig.getInstance().getBatchNo());
                                                                    TMConfig.getInstance().setBatchNo(val).save();
                                                                    TransLog.getInstance(idAcquirer).clearAll(idAcquirer);
                                                                    CommonFunctionalities.limpiarPanTarjGasolinera("");
                                                                }

                                                                idAcquirer = idLote + FILE_NAME_PREVOUCHER;
                                                                if (ToolsBatch.statusTrans(idAcquirer)) {
                                                                    if (idAcquirer != null) {
                                                                        if (ToolsBatch.statusTrans(idAcquirer)) {
                                                                            int val = Integer.parseInt(TMConfig.getInstance().getBatchNo());
                                                                            TMConfig.getInstance().setBatchNo(val).save();
                                                                            TransLog.getInstance(idAcquirer).clearAll(idAcquirer);
                                                                            CommonFunctionalities.limpiarPanTarjGasolinera("");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    };

                                                    idAcquirer = idLote;
                                                    if (idAcquirer != null) {
                                                        if (ToolsBatch.statusTrans(idAcquirer)) {
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            PrintParameter.setPrintTotals(true);
                                                            String id = menus.idAcquirer;
                                                            intent.setClass(context, PrintParameter.class);
                                                            intent.putExtra("typeReport", DefinesDATAFAST.ITEM_BORRAR_LOTE);
                                                            context.startActivity(intent);

                                                            //Actualiza la fecha de siguiente cierre si la trans que se esta guardando es la primerta del nuevo lote
                                                            CommonFunctionalities.saveDateSettle(context);

                                                            if (CommonFunctionalities.getFirtsTrans(context))
                                                                CommonFunctionalities.saveFirtsTrans(context, false);

                                                            UIUtils.toast((Activity) context, R.drawable.ic_launcher, "LOTE BORRADO", Toast.LENGTH_SHORT);

                                                            callBackSeatle = null;

                                                            callBackSeatle = new waitSeatleReport() {
                                                                @Override
                                                                public void getRspSeatleReport(int status) {

                                                                    if (status == 0) {
                                                                        try{

                                                                            UpdateApk updateApk = new UpdateApk(context);
                                                                            updateApk.instalarApp(context);

                                                                            if (readWriteFileMDM.getInitAuto().equals(readWriteFileMDM.INITAUTODEACTIVE)){
                                                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                intent.setClass(context, Init.class);
                                                                                intent.putExtra("PARCIAL", true);
                                                                                context.startActivity(intent);
                                                                            }
                                                                        }catch (Exception e){
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                }
                                                            };

                                                        } else {
                                                            callbackPrint.getRspPrintReport(0);
                                                            UIUtils.toast((Activity) context, R.drawable.ic_launcher, DefinesDATAFAST.LOTE_VACIO, Toast.LENGTH_SHORT);
                                                            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                                                            toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                                                        }
                                                    }
                                                }
                                            });

                                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });

                                    alertDialog.show();
                                }
                            }
                            break;
                        case DefinesDATAFAST.ITEM_ANULACION:
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setClass(context, MasterControl.class);
                            intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[22]);
                            context.startActivity(intent);
                            break;
                        case DefinesDATAFAST.ITEM_ANULACION_PRE_AUT:
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setClass(context, MasterControl.class);
                            intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[31]);
                            context.startActivity(intent);
                            break;
                        case DefinesDATAFAST.ITEM_REIMPRESION_PRE_AUT:
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setClass(context, MasterControl.class);
                            intent.putExtra(MasterControl.TRANS_KEY, PrintRes.TRANSEN[32]);
                            context.startActivity(intent);
                            break;
                        case DefinesDATAFAST.ITEM_CONFIGURACION:
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setClass(context, menus.class);
                            intent.putExtra(DefinesDATAFAST.DATO_MENU, DefinesDATAFAST.ITEM_COMUNICACION);
                            context.startActivity(intent);
                            break;
                    }
                    mDialog.dismiss();

                } else {
                    newEdit.setText("");
                    UIUtils.toast((Activity) context, R.drawable.ic_launcher, context.getString(R.string.err_msg_pwoperario), Toast.LENGTH_SHORT);
                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                    toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 500);
                }
            }
        });
    }
}
