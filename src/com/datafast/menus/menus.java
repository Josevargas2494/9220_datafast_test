package com.datafast.menus;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.desert.admanager.listener.OnItemClickListener;
import com.android.newpos.pay.R;
import com.datafast.definesDATAFAST.DefinesDATAFAST;
import com.datafast.inicializacion.tools.PolarisUtil;
import com.datafast.slide.slide;
import com.newpos.libpay.Logger;
import com.newpos.libpay.utils.ISOUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.master.MasterControl;

import static com.android.newpos.pay.StartAppDATAFAST.VERSION;
//import static com.android.newpos.pay.StartAppDATAFAST.batteryStatus;
import static com.android.newpos.pay.StartAppDATAFAST.isInit;
import static com.android.newpos.pay.StartAppDATAFAST.tconf;


/**
 * Copyright images
 * By Alfredo Hernandez
 * <div>Icons made by <a href="https://www.flaticon.com/authors/alfredo-hernandez" title="Alfredo Hernandez">Alfredo Hernandez</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
 */

public class menus extends AppCompatActivity implements OnItemClickListener {

    public static String idAcquirer;
    public static int contFallback = 0;
    Boolean isClose = true;
    boolean isDisplay;

    CountDownTimer countDownTimerDisplay;
    public static  CountDownTimer countDownTimerMenus;
    public static CountDownTimer countDownTimerSignature;
    public static CountDownTimer countDownTimer;
    public static CountDownTimer countDownTimerImg;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }

    private boolean activeDebugLocal = false;

    public static final int FALLBACK = 3;
    public static final int NO_FALLBACK = 0;
    public static final int TOTAL_BATCH = 300;

    private String menu;

    RelativeLayout layoutSaver;
    TextView version;
    private slide slide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus);
        RelativeLayout relativeLayoutBack = findViewById(R.id.relativeLayoutBack);
        layoutSaver = findViewById(R.id.layoutSaver);
        version = findViewById(R.id.textView_vrs);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        slide = new slide( menus.this, true);
        slide.galeria(this, R.id.adcolumn);
        slide.adColumn.setOnItemClickListener(this);

        //this.registerReceiver(batteryStatus, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        if (extras != null) {
            mostrarMenu(Objects.requireNonNull(extras.getString(DefinesDATAFAST.DATO_MENU)));
            menu = Objects.requireNonNull(extras.getString(DefinesDATAFAST.DATO_MENU));
            if (menu.equals(DefinesDATAFAST.ITEM_PRINCIPAL)) {
                relativeLayoutBack.setVisibility(View.INVISIBLE);
                getVersion();
            }
        }
    }

    private void getVersion() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        VERSION = packageInfo.versionName;
        //version.setText(StartAppDATAFAST.CERT + StartAppDATAFAST.VERSION);
        version.setText("V"+VERSION);
    }

    private void mostrarMenu(String tipoMenu) {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        if (tipoMenu.equals(DefinesDATAFAST.ITEM_TRANSACCIONES)) {
            String title = " ";
            toolbar.setTitle(Html.fromHtml(title));
        } else {
            String title = " ";
            toolbar.setTitle(Html.fromHtml(title));
        }

        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyItemMenu);

        RecyclerViewAdaptadorMenu recyclerViewAdaptadorMenu;
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //recyclerViewAdaptadorMenu = new RecyclerViewAdaptadorMenu(obtenerItems(tipoMenu), this, DefinesDATAFAST.TIPO_LAYOUT_LINEAR);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewAdaptadorMenu = new RecyclerViewAdaptadorMenu(obtenerItems(tipoMenu), this, DefinesDATAFAST.TIPO_LAYOUT_GRID);
        recyclerView.setAdapter(recyclerViewAdaptadorMenu);

        /*if (batteryStatus!=null)
            this.unregisterReceiver(batteryStatus);*/

    }

    public void onClickBack(View view) {
        if (!menu.equals(DefinesDATAFAST.ITEM_PRINCIPAL))
            finish();
    }

    public List<menuItemsModelo> obtenerItems(String tipoMenu) {
        List<menuItemsModelo> itemMenu = new ArrayList<>();

        switch (tipoMenu) {

            case DefinesDATAFAST.ITEM_PRINCIPAL:
                counterDownTimerDisplay();
                deleteTimerMenus();
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_TRANSACCIONES, R.drawable.ic_transacciones));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PAGOS_ELECTRONICOS, R.drawable.ic_pagoselectronicos));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_IMPRESION, R.drawable.ic_menuimpresion));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_COMERCIO, R.drawable.ic_comercio));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_CONFIGURACION, R.drawable.ic_configuracion));
                break;

            case DefinesDATAFAST.ITEM_TRANSACCIONES:
                counterDownTimerMenus();
                deleteTimerDisplay();
                if (isInit) {
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_VENTA()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_VENTA, R.drawable.ic_venta));
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_DIFERIDO()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_DIFERIDO, R.drawable.ic_diferido));
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_PRE_AUTO()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PRE_AUTORIZACION, R.drawable.ic_menupreautorizacion));
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_ANULACION()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_ANULACION, R.drawable.ic_anulacion));
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_PAGOS_VARIOS()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PAGOS_VARIOS, R.drawable.ic_pagosvarios));
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_PRE_VOUCHER())) {
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PRE_VOUCHER, R.drawable.ic_prevoucher));
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PAGO_PREVOUCHER, R.drawable.ic_prevoucher));
                    }
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_CASH_OVER()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_CASH_OVER, R.drawable.ic_cashover));
                    if (ISOUtil.stringToBoolean(tconf.getTRANSACCION_PAGOS_ELECTRONICOS()))
                        itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PAGOS_ELECTRONICOS, R.drawable.ic_pagoselectronicos));

                }else{
                    UIUtils.toast(menus.this, R.drawable.ic_launcher, "Debe Inicializar POS!", Toast.LENGTH_LONG);
                }
                break;

            case DefinesDATAFAST.ITEM_PRE_AUTORIZACION:
                counterDownTimerMenus();
                deleteTimerDisplay();
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_TRANS_PRE_AUT, R.drawable.ic_preauto));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_AMPLIACION, R.drawable.ic_ampliacionpre));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_CONFIRMACION, R.drawable.ic_confirmacion));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_ANULACION_PRE_AUT, R.drawable.ic_anulacionpre));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_REIMPRESION_PRE_AUT, R.drawable.ic_reimpresionpreauto));
                break;

            case DefinesDATAFAST.ITEM_IMPRESION:
                counterDownTimerMenus();
                deleteTimerDisplay();
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_RE_IMPRESION, R.drawable.ic_reimpresionpreauto));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_REPORTE_DETALLADO, R.drawable.ic_reportedetallado));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_TEST, R.drawable.ic_test));
                break;

            case DefinesDATAFAST.ITEM_RE_IMPRESION:
                counterDownTimerMenus();
                deleteTimerDisplay();
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_TRANS_EN_PANTALLA, R.drawable.ic_menuimpresion));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_PREAUTO_PANTALLA, R.drawable.ic_menuimpresion));
                break;

            case DefinesDATAFAST.ITEM_COMERCIO:
                counterDownTimerMenus();
                deleteTimerDisplay();
                if (ISOUtil.stringToBoolean(tconf.getHABILITA_CIERRE())){
                    itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_DEPOSITO, R.drawable.ic_reportedetallado));
                }
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_ECHO_TEST, R.drawable.ic_echo));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_BORRAR_LOTE, R.drawable.ic_borrarlote));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_BORRAR_REVERSO, R.drawable.ic_borrarreverso));
                break;

            case DefinesDATAFAST.ITEM_COMUNICACION:
                counterDownTimerMenus();
                deleteTimerDisplay();
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_INICIALIZACION, R.drawable.ic_polaris));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_CONFIG_INICIAL, R.drawable.ic_configuracion));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_CONFIG_WIFI, R.drawable.ic_wifi));
                itemMenu.add(new menuItemsModelo(DefinesDATAFAST.ITEM_APPMANAGER, R.drawable.ic_appmanager));
                break;
        }

        return itemMenu;
    }

    private void counterDownTimerDisplay() {
        if (countDownTimerDisplay != null) {
            countDownTimerDisplay.cancel();
            countDownTimerDisplay = null;
        }
        countDownTimerDisplay = new CountDownTimer(60000 * 3, 1000) {
            public void onTick(long millisUntilFinished) {
                if (activeDebugLocal)
                    Log.d("onTick", "init onTick countDownTimer Display");
                //Toast.makeText(menus.this,"counterDownTimerDisplay seconds remaining: " + millisUntilFinished / 1000,Toast.LENGTH_SHORT).show();
                if (isDisplay) {
                    layoutSaver.setVisibility(View.VISIBLE);
                    layoutSaver.setBackgroundColor(Color.parseColor("#7F000000"));
                }
            }

            public void onFinish() {
                if (activeDebugLocal)
                    Log.d("onTick", "finish onTick countDownTimer Display");
                //Toast.makeText(menus.this,"onTick countDownTimer Display",Toast.LENGTH_SHORT).show();
                isDisplay = true;
                slide.setTimeoutSlide(5000);
                deleteTimerDisplay();
                counterDownTimerDisplay();
            }
        }.start();
    }

    private void deleteTimerDisplay() {
        if (countDownTimerDisplay != null) {
            countDownTimerDisplay.cancel();
            countDownTimerDisplay = null;
        }
    }

    private void counterDownTimerMenus() {
        //stopAsyncTask();
        if (countDownTimerMenus != null) {
            countDownTimerMenus.cancel();
            countDownTimerMenus = null;
        }
        layoutSaver.setClickable(false);
        countDownTimerMenus = new CountDownTimer(35000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (activeDebugLocal)
                    Log.d("onTick", "init onTick countDownTimer HomeDataFast");
                //Toast.makeText(menus.this,"countDownTimerMenus seconds remaining: " + millisUntilFinished / 1000,Toast.LENGTH_SHORT).show();
            }

            public void onFinish() {
                if (activeDebugLocal)
                    Log.d("onTick", "finish onTick countDownTimer HomeDataFast");
                //Toast.makeText(menus.this,"onTick countDownTimer HomeDataFast",Toast.LENGTH_SHORT).show();
                deleteTimerMenus();
                deleteTimerDisplay();
                finish();
            }
        }.start();
    }

    public static void deleteTimerMenus() {
        if (countDownTimerMenus != null) {
            countDownTimerMenus.cancel();
            countDownTimerMenus = null;
        }
    }

    public void onClickCloseDisplay() {
        deleteTimerDisplay();
        isDisplay = false;
        slide.stopSlide();
        counterDownTimerDisplay();
        layoutSaver.setVisibility(View.GONE);
        setBrightness(140);
    }

    public void setBrightness(int brightness1) {
        //Getting Current screen brightness.
        int brightness = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0
        );
        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness1);
    }

    @Override
    public void onBackPressed() {
        if (!menu.equals(DefinesDATAFAST.ITEM_PRINCIPAL)) {
            deleteTimerDisplay();
            super.onBackPressed();

            if (isClose && !isDisplay)
                finish();
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        deleteTimerMenus();
                        deleteTimerDisplay();
                    }
                });
            }
        }
    }

    public static boolean pause = false;

    @Override
     protected void onResume() {
         super.onResume();
         contFallback = 0;
         isInit = PolarisUtil.isInitPolaris(menus.this);
         stopTimers();
         if (pause) {
             pause = false;
             counterDownTimerMenus();
         }
     }

    //--------------------------------- Venta ---------------------------------

    /**
     * Permite ejecutar la trans seleccionada
     */
    public static void startTrans(Context ctx, String ch) {
        Logger.debug("startTrans:" + ch);
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClass(ctx, MasterControl.class);
        intent.putExtra(MasterControl.TRANS_KEY, ch);
        ctx.startActivity(intent);
    }

    @Override
    public void onItemClick(int i) {
        onClickCloseDisplay();
    }

    private void stopTimers(){
        if (countDownTimerSignature != null) {
            //Toast.makeText(menus.this,"countDownTimerSignature",Toast.LENGTH_SHORT).show();
            countDownTimerSignature.cancel();
            countDownTimerSignature = null;
        }

        if (countDownTimer != null) {
            //Toast.makeText(menus.this,"countDownTimer",Toast.LENGTH_SHORT).show();
            countDownTimer.cancel();
            countDownTimer = null;
        }

        if (countDownTimerImg != null) {
            //Toast.makeText(menus.this,"countDownTimerImg",Toast.LENGTH_SHORT).show();
            countDownTimerImg.cancel();
            countDownTimerImg = null;
        }
    }
}

