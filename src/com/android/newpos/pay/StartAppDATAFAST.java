package com.android.newpos.pay;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.widget.Toast;

import com.datafast.definesDATAFAST.DefinesDATAFAST;
import com.datafast.inicializacion.configuracioncomercio.ChequeoIPs;
import com.datafast.inicializacion.configuracioncomercio.Host_Confi;
import com.datafast.inicializacion.configuracioncomercio.IP;
import com.datafast.inicializacion.configuracioncomercio.Rango;
import com.datafast.inicializacion.configuracioncomercio.TCONF;
import com.datafast.inicializacion.pagosvarios.PagosVarios;
import com.datafast.inicializacion.prompts.Prompt;
import com.datafast.inicializacion.tools.PolarisUtil;
import com.datafast.inicializacion.trans_init.Init;
import com.datafast.keys.InjectMasterKey;
import com.datafast.keys.PwMasterKey;
import com.datafast.mdm_validation.ReadWriteFileMDM;
import com.datafast.menus.menus;
import com.datafast.tools.BatteryStatus;
import com.datafast.tools.Estadisticas;
import com.datafast.tools.PaperStatus;
import com.datafast.tools_card.GetCard;
import com.datafast.transactions.callbacks.makeInitCallback;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.global.TMConfig;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.base.PayApplication;

import static com.datafast.keys.InjectMasterKey.MASTERKEYIDX;
import static com.datafast.keys.InjectMasterKey.threreIsKey;


public class StartAppDATAFAST extends AppCompatActivity {

    public static String VERSION = "";
    public static final String CERT = "CERTIFICACION   ";
    public static BatteryStatus batteryStatus;
    public static PaperStatus paperStatus;

    //public static GetCard getCard = null;

    public static TCONF tconf = null;
    public static Host_Confi host_confi = null;
    public static IP tablaIp = null;
    public static Rango rango = null;
    public static ArrayList<IP> listIPs = null;
    public static ArrayList<Prompt> listPrompts = null;
    public static ArrayList<PagosVarios> listPagosVarios = null;
    public static boolean isInit = false;
    public static ReadWriteFileMDM readWriteFileMDM = null;
    public static makeInitCallback makeinitcallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (idioma()){
            initSDK();

            //Estadisticas.createFile(StartAppDATAFAST.this);
            //Active debug mode
            //TMConfig.getInstance().activeDebugMode(false);

            isInit = PolarisUtil.isInitPolaris(StartAppDATAFAST.this);


            paperStatus = new PaperStatus();

            /*if (readWriteFileMDM == null) {
                readWriteFileMDM = ReadWriteFileMDM.getInstance();
            }*/

            batteryStatus = new BatteryStatus();
            this.registerReceiver(batteryStatus, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }

    private boolean idioma() {
        boolean ret = false;
        String idiomaLocal = Locale.getDefault().toString();
        if (!idiomaLocal.equals("es_US")){
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light_Dialog));

            builder.setIcon(R.drawable.ic_launcher);
            builder.setTitle("Advertencia");
            builder.setMessage("Por favor cambia el idioma del dispositivo.\nPreferencia: Español - Estados Unidos");
            builder.setCancelable(false);

            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(Settings.ACTION_LOCALE_SETTINGS));
                }
            });
            Dialog dialog = builder.create();
            dialog.show();
        } else {
            ret = true;
        }
        return ret;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (idioma()){
            //InjectMasterKey.deleteKeys(KeyType.KEY_TYPE_MASTK, MASTERKEYIDX);
            if (threreIsKey(MASTERKEYIDX, "Debe cargar Master Key", StartAppDATAFAST.this)){
                initObjetPSTIS();
                leerBaseDatos(StartAppDATAFAST.this);
            }else{
                inyectarLlaves();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*try {
            if (batteryStatus!=null)
                this.unregisterReceiver(batteryStatus);
        }
        catch (final Exception exception) {
            // The receiver was not registered.
            // There is nothing to do in that case.
            // Everything is fine.
        }*/

    }

    /**
     * inicializa el sdk
     */
    private void initSDK() {
        PaySdk.getInstance().setActivity(this);
        PayApplication.getInstance().addActivity(this);
        PayApplication.getInstance().setRunned();
    }

    /**
     * Instancia todos los objetos necesarios para el manejo de la
     * inicializacion del PSTIS
     */
    private void initObjetPSTIS(){

        //----------- Init DataFast-----------
        if (host_confi == null){
            host_confi = Host_Confi.getSingletonInstance();
        }

        if (listIPs == null){
            listIPs = new ArrayList<>();
        }

        if (tablaIp == null){
            tablaIp = new IP();
        }

        if (tconf == null){
            tconf = TCONF.getSingletonInstance();
        }

        if (rango == null){
            rango = Rango.getSingletonInstance();
        }

        //--------- limpiar datos----------
        if (tconf != null){
            tconf.clearTCONF();
        }
        if (rango != null){
            rango.clearRango();
        }
        if (host_confi != null){
            host_confi.clearHost_Confi();
        }

        if (listIPs != null){
            listIPs.clear();
        }
    }

    /**
     * Permite realizar la inyeccion de la masterkey
     * Por motivo de pruebas se deja de manera fija,
     * pero esta debe ser inyectada desde una tarjeta chip
     */
    private void inyectarLlaves(){
        //initMasterKey();
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClass(StartAppDATAFAST.this, PwMasterKey.class);
        startActivity(intent);
    }

    public void leerBaseDatos(Context context){
        if (isInit) {
            tconf.selectTconf(context);
            host_confi.selectHostConfi(context);

            listIPs = ChequeoIPs.selectIP(context);
            if (listIPs == null) {
                isInit = false;
                UIUtils.toast((Activity) context, R.drawable.ic_launcher, "Error al leer tabla, Por favor Inicialice nuevamente", Toast.LENGTH_LONG);
            }

            if (readWriteFileMDM == null){
                try {
                    readWriteFileMDM = ReadWriteFileMDM.getInstance();

                    if (readWriteFileMDM.getReverse().equals(readWriteFileMDM.REVERSE_DEACTIVE) &&
                            readWriteFileMDM.getSettle().equals(readWriteFileMDM.TRANS_DEACTIVE) &&
                            readWriteFileMDM.getInitAuto().equals(readWriteFileMDM.INITAUTOACTIVE)) {

                        makeinitcallback = null;

                        makeinitcallback = new makeInitCallback() {
                            @Override
                            public void getMakeInitCallback(boolean status) {
                                if (status) {
                                    initApp();
                                }
                            }
                        };

                        Thread t = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    long start = SystemClock.uptimeMillis() ;

                                    //check if connected!
                                    while (!isConnected()) {
                                        //Wait to connect
                                        Thread.sleep(1000);

                                        if (SystemClock.uptimeMillis() - start > 15000) {
                                            break;
                                        }
                                    }

                                    Intent intent = new Intent();
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setClass(StartAppDATAFAST.this, Init.class);
                                    intent.putExtra("PARCIAL", false);
                                    startActivity(intent);

                                } catch (Exception e) {
                                    initApp();
                                }
                            }
                        };
                        t.start();

                    } else {
                        makeinitcallback = null;
                        initApp();
                    }
                } catch (Exception e){
                    initApp();
                }
            }else{
                initApp();
            }
        }else{
            initApp();
        }
    }

    /**
     * Inicio de la app
     */
    private void initApp(){
        Intent intent = new Intent();
        intent.setClass(StartAppDATAFAST.this, menus.class);
        intent.putExtra(DefinesDATAFAST.DATO_MENU, DefinesDATAFAST.ITEM_PRINCIPAL);
        startActivity(intent);
    }

    private boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }
}
