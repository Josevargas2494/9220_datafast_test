package com.newpos.libpay.device.card;

import com.android.desert.keyboard.InputManager;
import com.newpos.libpay.device.user.OnUserResultListener;
import com.newpos.libpay.presenter.TransView;
import com.newpos.libpay.trans.Tcode;

public class ManualCardEntry {

    private static ManualCardEntry mInstance = null;
    private int mRet = 0;
    private InputManager.Style payStyle;

    public static ManualCardEntry getInstance() {
        if (mInstance==null){
            mInstance = new ManualCardEntry();
        }

        return mInstance;
    }

    /**
     * object lock
     */
    private Object o = new byte[0] ;

    /**
     * Notify
     */
    private void listenNotify(){
        synchronized (o){
            o.notify();
        }
    }

    /**
     * block
     */
    private void funWait(){
        synchronized (o){
            try {
                o.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private final OnUserResultListener listener = new OnUserResultListener() {
        @Override
        public void confirm(InputManager.Style style) {
            mRet = 0;
            payStyle = style;
            //mLatch.countDown();
            listenNotify();
        }

        @Override
        public void cancel() {
            mRet = 1;
            //mLatch.countDown();
            listenNotify();
        }

        @Override
        public void confirm(int applistselect) {
            mRet = applistselect;
            //mLatch.countDown();
            listenNotify();
        }
    };


    /**
     *
     * Not used timeout
     * @param transView
     * @param handleCardCallback
     */
    public void startSearchCard(TransView transView, final HandleCardManager handleCardCallback){

        transView.getResultListener(listener);

        final CardInfo cInfo = new CardInfo();

        funWait();

        if (mRet == 1) {
            cInfo.setResultFalg(false);
            cInfo.setErrno(Tcode.T_user_cancel_operation);
            handleCardCallback.onSearchResult(HandleCardManager.USER_CANCEL,cInfo);
        }else {
            cInfo.setResultFalg(true);
            cInfo.setCardType(CardManager.TYPE_HAND);
            handleCardCallback.onSearchResult(HandleCardManager.SUCCESS,cInfo);
        }
    }
}
